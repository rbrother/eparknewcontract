using System;
using System.Collections.Generic;

namespace ePark.Data
{
    public class EmailStatuMeta
    {
        public int EmailStatusID { get; set; }
        public string EmailStatusDesc { get; set; }
        public Nullable<bool> IsActive { get; set; }
    
        public virtual ICollection<EmailTran> EmailTrans { get; set; }
    }
}
