using System;
using System.Collections.Generic;

namespace ePark.Data
{
    public class ReceiptMeta
    {    
        public int ReceiptID { get; set; }
        public Nullable<int> ReceiptPayStatusID { get; set; }
        public Nullable<int> ReceiptPayTypeID { get; set; }
        public Nullable<int> BillingCardTypesID { get; set; }
        public Nullable<System.DateTime> PayDate { get; set; }
        public string ReceiptNumber { get; set; }
        public Nullable<decimal> ReceiptTotalAmount { get; set; }
        public Nullable<decimal> ConvFee { get; set; }
        public Nullable<decimal> Tax { get; set; }
        public Nullable<decimal> ConvRate { get; set; }
    
        public virtual BillingCardType BillingCardType { get; set; }
       // public virtual ICollection<ParkingContractTransaction> ParkingContractTransactions { get; set; }
        public virtual ICollection<PaymentTransactionLog> PaymentTransactionLogs { get; set; }
        public virtual ReceiptPayStatu ReceiptPayStatu { get; set; }
        public virtual ReceiptPayType ReceiptPayType { get; set; }
    }
}
