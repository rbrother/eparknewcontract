namespace ePark.Data
{   
    public class AspNetUserLoginMeta
    {
        public string LoginProvider { get; set; }
        public string ProviderKey { get; set; }
        public int UserId { get; set; }
    
        public virtual AspNetUser AspNetUser { get; set; }
    }
}
