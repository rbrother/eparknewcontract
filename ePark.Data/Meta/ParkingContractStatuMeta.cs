using System;
using System.Collections.Generic;

namespace ePark.Data
{
    public class ParkingContractStatuMeta
    {
        public int ParkingContractStatusID { get; set; }
        public string ParkingContractStatusDesc { get; set; }
        public Nullable<bool> IsActive { get; set; }
    
        public virtual ICollection<AspNetUsersContractXref> AspNetUsersContractXrefs { get; set; }
    }
}
