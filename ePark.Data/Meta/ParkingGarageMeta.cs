using System;
using System.Collections.Generic;

namespace ePark.Data
{
    public class ParkingGarageMeta
    {    
        public int VenueID { get; set; }
        public Nullable<int> ParkingEntityID { get; set; }
        public string ParkingGarageDesc { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
    
        public virtual ParkingEntity ParkingEntity { get; set; }
        public virtual ICollection<ParkingLevel> ParkingLevels { get; set; }
    }
}
