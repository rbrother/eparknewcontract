using System;
using System.Collections.Generic;

namespace ePark.Data
{
    public class ParkingContractRateMeta
    {   
        public int ParkingContractRatesID { get; set; }
        public Nullable<int> ParkingContractID { get; set; }
        public Nullable<System.DateTime> ParkingContractRateBegin { get; set; }
        public Nullable<System.DateTime> ParkingContractRateEnd { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<bool> IsActive { get; set; }
    
        public virtual ICollection<ParkingContractTransaction> ParkingContractTransactions { get; set; }
    }
}
