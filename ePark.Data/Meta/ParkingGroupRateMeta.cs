using System;
using System.Collections.Generic;

namespace ePark.Data
{
    public class ParkingGroupRateMeta
    {
        public int Parking_GroupRatesID { get; set; }
        public Nullable<int> ParkingGroupID { get; set; }
        public Nullable<System.DateTime> ParkingGroupRateBegin { get; set; }
        public Nullable<System.DateTime> ParkingGroupRateEnd { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<bool> IsActive { get; set; }
    
        public virtual ParkingGroup ParkingGroup { get; set; }
    }
}
