using System;
using System.Collections.Generic;

namespace ePark.Data
{
    public class ParkingContractDocumentMeta
    {   
        public int ParkingContractDocumentID { get; set; }
        public Nullable<int> ParkingContractID { get; set; }
        public string ContractFilePath { get; set; }
        public Nullable<System.Guid> ContractFile { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
    
        public virtual ICollection<AspNetUsersContractXref> AspNetUsersContractXrefs { get; set; }
        public virtual ParkingContract ParkingContract { get; set; }
    }
}
