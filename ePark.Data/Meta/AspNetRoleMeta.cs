using System.Collections.Generic;

namespace ePark.Data
{   
    public class AspNetRoleMeta
    {    
        public int Id { get; set; }
        public string Name { get; set; }
    
        public virtual ICollection<AspNetUser> AspNetUsers { get; set; }
    }
}
