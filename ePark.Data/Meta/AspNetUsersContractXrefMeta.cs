using System;

namespace ePark.Data
{
    public class AspNetUsersContractXrefMeta
    {
        public int AspNetUsersContractXrefID { get; set; }
        public Nullable<int> AspNetUsersID { get; set; }
        public Nullable<int> ParkingContractID { get; set; }
        public Nullable<int> ParkingContractStatusID { get; set; }
        public string FilePath { get; set; }
        public Nullable<System.Guid> FileName { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<bool> IsCurrent { get; set; }
 
        public virtual AspNetUser AspNetUser { get; set; }
        public virtual ParkingContractDocument ParkingContractDocument { get; set; }
        public virtual ParkingContractStatu ParkingContractStatu { get; set; }
    }
}
