using System;
using System.Collections.Generic;
    
namespace ePark.Data
{
    public class ParkingEntityMeta
    {    
        public int ParkingEntityID { get; set; }
        public string ParkingEntityDesc { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
    
        public virtual ICollection<ParkingGarage> ParkingGarages { get; set; }
    }
}
