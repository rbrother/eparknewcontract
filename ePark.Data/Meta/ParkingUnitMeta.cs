using System;
using System.Collections.Generic;

namespace ePark.Data
{
    public class ParkingUnitMeta
    {
        public int ParkingUnitID { get; set; }
        public Nullable<int> ParkingLevelID { get; set; }
        public string ParkingUnitDesc { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<bool> IsAvailable { get; set; }
        public Nullable<bool> IsActive { get; set; }
    
        public virtual ParkingLevel ParkingLevel { get; set; }
    }
}
