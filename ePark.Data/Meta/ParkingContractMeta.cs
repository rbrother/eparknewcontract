using System;
using System.Collections.Generic;
    
namespace ePark.Data
{
    public class ParkingContractMeta
    {
        public int ParkingContractID { get; set; }
        public Nullable<int> AspNetUserID { get; set; }
        public Nullable<int> ParkingEntityID { get; set; }
        public Nullable<int> ParkingGroupID { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
    
        public virtual AspNetUser AspNetUser { get; set; }
        public virtual ParkingGroup ParkingGroup { get; set; }
        public virtual ICollection<ParkingContractDocument> ParkingContractDocuments { get; set; }
        public virtual ICollection<ParkingContractTransaction> ParkingContractTransactions { get; set; }
    }
}
