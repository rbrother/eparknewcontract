using System;
using System.Collections.Generic;
    
namespace ePark.Data
{
    public class ParkingGroupCategoryMeta
    {    
        public int ParkingGroupCategoryID { get; set; }
        public string ParkingGroupCategoryDesc { get; set; }
        public Nullable<bool> IsActive { get; set; }
    
        public virtual ICollection<ParkingGroup> ParkingGroups { get; set; }
    }
}
