using System;
using System.Collections.Generic;

namespace ePark.Data
{    
    public class PaymentTransactionLogMeta
    {
        public int transLogID { get; set; }
        public string transCardType { get; set; }
        public string transRequestType { get; set; }
        public Nullable<System.DateTime> transDate { get; set; }
        public string transMerchantCode { get; set; }
        public string transSettleCode { get; set; }
        public Nullable<int> transLast5Digits { get; set; }
        public Nullable<double> transAmount { get; set; }
        public Nullable<decimal> transConvFee { get; set; }
        public Nullable<int> transExpMM { get; set; }
        public Nullable<int> transExpYY { get; set; }
        public Nullable<bool> transBillAddressSent { get; set; }
        public string transCardSecurityValue { get; set; }
        public Nullable<bool> transDataSent { get; set; }
        public Nullable<int> transReturnCode { get; set; }
        public string transResponseString { get; set; }
        public string transTransactionID { get; set; }
        public Nullable<System.DateTime> transDateStamp { get; set; }
        public string transAuthorization { get; set; }
        public string transStatus { get; set; }
        public string transUser1 { get; set; }
        public string transUser2 { get; set; }
        public string transUser3 { get; set; }
        public string transQuery { get; set; }
        public Nullable<int> TransReceiptID { get; set; }
    
        public virtual Receipt Receipt { get; set; }
    }
}
