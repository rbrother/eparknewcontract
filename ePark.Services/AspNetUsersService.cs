﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;

namespace ePark.Services
{
    public class AspNetUsersService : IDisposable
    {
         private readonly EparkContext _db;

         public AspNetUsersService()
        
         {
            
             _db = new EparkContext();
        }

         public AspNetUser GetById(int id)
         {
             // if not found will return null
             return _db.AspNetUsers.FirstOrDefault(x => x.Id == id);
         }

         public AspNetUser GetBySECStamp(string secstamp)
         {
             // if not found will return null
             return _db.AspNetUsers.FirstOrDefault(x => x.SecurityStamp == secstamp);
         }


         public AspNetUser GetByUserName(string username)
         {
             // if not found will return null
             return _db.AspNetUsers.FirstOrDefault(x => x.UserName == username);
         }


         public AspNetUser GetBySecurityStamp(string securityStamp)
         {
             // if not found will return null
             return _db.AspNetUsers.FirstOrDefault(x => x.SecurityStamp == securityStamp);
         }

         public List<AspNetUser> GetList()
         {
             return _db.AspNetUsers.ToList();
         }

         public int Create(AspNetUser aspNetUser)
         {
             var newaspNetUser = new AspNetUser
             {
                Id = aspNetUser.Id,
	            AspNetUserProfile_Id = aspNetUser.AspNetUserProfile_Id,
	            Email = aspNetUser.Email,
	            EmailConfirmed = aspNetUser.EmailConfirmed,
	            PasswordHash = aspNetUser.PasswordHash,
	            SecurityStamp = aspNetUser.SecurityStamp,
	            PhoneNumber = aspNetUser.PhoneNumber,
	            PhoneNumberConfirmed = aspNetUser.PhoneNumberConfirmed,
	            TwoFactorEnabled = aspNetUser.TwoFactorEnabled,
	            LockoutEndDateUtc = aspNetUser.LockoutEndDateUtc,
	            LockoutEnabled = aspNetUser.LockoutEnabled,
	            AccessFailedCount = aspNetUser.AccessFailedCount,
	            UserName = aspNetUser.UserName

             };

             _db.AspNetUsers.Add(newaspNetUser);

             return _db.SaveChanges();
         }

         public int Update(AspNetUser aspNetUser)
         {
             var existingAspNetUser = _db.AspNetUsers.FirstOrDefault(x => x.Id == aspNetUser.Id);

             if (existingAspNetUser != null)
             {
                 existingAspNetUser.AspNetUserProfile_Id = aspNetUser.AspNetUserProfile_Id;
                 existingAspNetUser.Email = aspNetUser.Email;
                 existingAspNetUser.EmailConfirmed = aspNetUser.EmailConfirmed;
                 existingAspNetUser.PasswordHash = aspNetUser.PasswordHash;
                 existingAspNetUser.SecurityStamp = aspNetUser.SecurityStamp;
                 existingAspNetUser.PhoneNumber = aspNetUser.PhoneNumber;
                 existingAspNetUser.PhoneNumberConfirmed= aspNetUser.PhoneNumberConfirmed;
                 existingAspNetUser.TwoFactorEnabled = aspNetUser.TwoFactorEnabled;
                 existingAspNetUser.LockoutEndDateUtc = aspNetUser.LockoutEndDateUtc;
                 existingAspNetUser.LockoutEnabled = aspNetUser.LockoutEnabled;
                 existingAspNetUser.AccessFailedCount = aspNetUser.AccessFailedCount;
                 existingAspNetUser.UserName = aspNetUser.UserName;

                 return _db.SaveChanges();
             }

             return 0;
         }

         public int Delete(AspNetUser aspNetUser)
         {
             var existingAspNetUser = _db.AspNetUsers.FirstOrDefault(x => x.Id == aspNetUser.Id);

             if (existingAspNetUser != null)
             {
                 _db.AspNetUsers.Remove(existingAspNetUser);
                 return _db.SaveChanges();
             }

             return 0;
         }

         public int DeleteById(int aspNetUserID)
         {
             var existingAspNetUser = _db.AspNetUsers.FirstOrDefault(x => x.Id == aspNetUserID);

             if (existingAspNetUser != null)
             {
                 _db.AspNetUsers.Remove(existingAspNetUser);
                 return _db.SaveChanges();
             }

             return 0;
         }

          public void Dispose()
        {
            ((IDisposable)_db).Dispose();
        }

    }
}
