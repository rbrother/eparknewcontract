﻿using System;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;

namespace ePark.Services
{
    public class vwPaymentReceiptsService : IDisposable
    {
        private readonly EparkContext _db;
        public vwPaymentReceiptsService()
        {
            _db = new EparkContext();
        }

         public List<vwPaymentReceipt> GetList()
         {
             // if not found will return null
             return _db.vwPaymentReceipts.ToList();
         }

         public List<vwPaymentReceipt> GetListByReceiptId(int id)
         {
             // if not found will return null
             List<vwPaymentReceipt> receipts = _db.vwPaymentReceipts.Where(x => x.ReceiptID == id).ToList();
             return receipts;
             // return _db.vwReceipts.Where(x => x.ReceiptID == id).ToList();
         }

         public void Dispose()
         {
             ((IDisposable)_db).Dispose();
         }
    }
}
