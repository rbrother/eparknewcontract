﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;

namespace ePark.Services
{
    public class vwCustomerPaymentService : IDisposable
    {
         private readonly EparkContext _db;

         public vwCustomerPaymentService()
        {
            _db = new EparkContext();
        }

         public List<vwCustomerPayment> GetList()
         {
             return _db.vwCustomerPayments.ToList();
         }

         public List<vwCustomerPayment> GetByPaymentType(string pymnttype)
         {
             return _db.vwCustomerPayments.Where(x => x.transQuery == pymnttype).ToList();
         }

         public List<vwCustomerPayment> GetByAspNetUserId(int userid)
         {
             return _db.vwCustomerPayments.Where(x => x.Id == userid).ToList();
         }
        
         public List<vwCustomerPayment> GetListByAspNetUserId(int userid)
         {
             return _db.vwCustomerPayments.Where(x => x.Id == userid).ToList();
         }

         public List<vwCustomerPayment> GetByContractId(int contractid)
         {
             return _db.vwCustomerPayments.Where(x => x.transUser3 == contractid.ToString()).ToList();
         }


        public void Dispose()
        {
            ((IDisposable)_db).Dispose();
        }
    }
}
