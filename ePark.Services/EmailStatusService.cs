﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;

namespace ePark.Services
{
    public class EmailStatusService : IDisposable
    {
         private readonly EparkContext _db;

         public EmailStatusService()
        {
            _db = new EparkContext();
        }

        
        public EmailStatu GetById(int emailStatusID)
        {
            // if not found will return null
            return _db.EmailStatus.FirstOrDefault(x => x.EmailStatusID == emailStatusID);
        }

        public List<EmailStatu> GetList()
        {
            return _db.EmailStatus.ToList();
        }
      
        public int Create(EmailStatu emailStatu)
        {
            var newemailStatu = new EmailStatu
            {
                EmailStatusID = emailStatu.EmailStatusID,
                EmailStatusDesc = emailStatu.EmailStatusDesc,
                IsActive = emailStatu.IsActive,
                
            };

            _db.EmailStatus.Add(newemailStatu);

            return _db.SaveChanges();
        }

        public int Update(EmailStatu emailStatu)
        {
            var existingEmailStatus = _db.EmailStatus.FirstOrDefault(x => x.EmailStatusID == emailStatu.EmailStatusID);

            if (existingEmailStatus != null)
             {
                 existingEmailStatus.EmailStatusID = emailStatu.EmailStatusID;
                 existingEmailStatus.EmailStatusDesc = emailStatu.EmailStatusDesc;
                 existingEmailStatus.IsActive = emailStatu.IsActive;

                return _db.SaveChanges();
              }

              return 0;
          }

        public int Delete(EmailStatu emailStatu)
        {
            var existingEmailStatu = _db.EmailTrans.FirstOrDefault(x => x.EmailStatusID == emailStatu.EmailStatusID);

            if (existingEmailStatu != null)
            {
                _db.EmailTrans.Remove(existingEmailStatu);
                return _db.SaveChanges();
            }

            return 0;
        }

        public int DeleteById(int emailStatusID)
        {
            var existingEmailStatus = _db.EmailTrans.FirstOrDefault(x => x.EmailTransID == emailStatusID);

            if (existingEmailStatus != null)
            {
                _db.EmailTrans.Remove(existingEmailStatus);
                return _db.SaveChanges();
            }

            return 0;
        }
        public void Dispose()
        {
            ((IDisposable)_db).Dispose();
        }


    }
}
