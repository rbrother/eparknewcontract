﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using ePark.Data;


namespace ePark.Services
{
    public class GroupCategoryRateViewService :IDisposable
    {
         private readonly EparkContext _db;
         public GroupCategoryRateViewService()
        {
            _db = new EparkContext();
        }


         public List<vwGroupCategoryRate> GetList()
         {
             // if not found will return null
             return _db.vwGroupCategoryRates.ToList();
         }

         public vwGroupCategoryRate GetByGroupIdList(int GroupId)
         {
             // if not found will return null
             return _db.vwGroupCategoryRates.FirstOrDefault(x => x.GroupID == GroupId);
         }



         public void Dispose()
         {
             ((IDisposable)_db).Dispose();
         }
    }
}
