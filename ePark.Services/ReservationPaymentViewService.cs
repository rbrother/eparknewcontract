﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using ePark.Data;

namespace ePark.Services
{
    public class ReservationPaymentViewService : IDisposable
    {
          private readonly EparkContext _db;
          public ReservationPaymentViewService()
        {
            _db = new EparkContext();
        }

          public vwReservationPayment GetByReceiptId(int receiptId)
        {
            // if not found will return null
            return _db.vwReservationPayments.FirstOrDefault(x => x.ReceiptID == receiptId);
        }


          public List<vwReservationPayment> GetByContractId(int ContractId)
          {
              // if not found will return null
              return _db.vwReservationPayments.Where(x => x.ContractID == ContractId).ToList();
          }

          public vwReservationPayment GetByContractTransactionVwReservationPaymentId(int contracttransactionid)
          {
              // if not found will return null
              return _db.vwReservationPayments.FirstOrDefault(x => x.ContractTransactionID == contracttransactionid);
          }

          public List<vwReservationPayment> GetList()
        {
            // if not found will return null
            return _db.vwReservationPayments.ToList();
        }

          public List<vwReservationPayment> GetListByUserId(int UserId)
          {
              // if not found will return null
              return _db.vwReservationPayments.Where(x => x.AspNetUserID == UserId).ToList();
          }

          public List<vwReservationPayment> GetOpenContractTrans()
          {
              // if not found will return null
              return _db.vwReservationPayments.Where(x => x.Paid == false).ToList();
              
          }

          public List<vwReservationPayment> GetUnPaidContractTrans()
          {
              // if not found will return null
              return _db.vwReservationPayments.Where(x => x.Paid == false).ToList();

          }

          public List<vwReservationPayment> GetPaidContractTrans()
          {
              // if not found will return null
              return _db.vwReservationPayments.Where(x => x.Paid == true).ToList();

          }


          public List<vwReservationPayment> GetUnPaidForUserContractTrans(int userId)
          {
              // if not found will return null
              return _db.vwReservationPayments.Where(x => x.Paid == false && x.AspNetUsersId == userId).ToList();

          }
         public void Dispose()
        {
            ((IDisposable)_db).Dispose();
        }
    }

    
}
