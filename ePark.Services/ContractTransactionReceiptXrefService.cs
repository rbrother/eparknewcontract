﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;

namespace ePark.Services
{
    public class ContractTransactionReceiptXrefService : IDisposable
    {
         private readonly EparkContext _db;
         public ContractTransactionReceiptXrefService()
        {
            _db = new EparkContext();
        }

         public ContractTransactionReceiptXref GetById(int XrefID)
         {
             // if not found will return null
             return _db.ContractTransactionReceiptXrefs.FirstOrDefault(x => x.ContractTransactionID == XrefID);
         }

         public ContractTransactionReceiptXref GetByContractTransactionID(int TransID)
         {
             // if not found will return null
             return _db.ContractTransactionReceiptXrefs.FirstOrDefault(x => x.ContractTransactionID == TransID);
         }

         public ContractTransactionReceiptXref GetByReceiptID(int ReceiptID)
         {
             // if not found will return null
             return _db.ContractTransactionReceiptXrefs.FirstOrDefault(x => x.ContractTransactionID == ReceiptID);
         }

         public int Create(ContractTransactionReceiptXref Xref)
         {
             var newXref = new ContractTransactionReceiptXref
             {
                 ContractTransactionID = Xref.ContractTransactionID,
                 ReceiptID = Xref.ReceiptID,
                 CreateDate = Xref.CreateDate,
                 IsActive = Xref.IsActive

             };

             _db.ContractTransactionReceiptXrefs.Add(newXref);

             return _db.SaveChanges();
         }

         public int Update(ContractTransactionReceiptXref Xref)
         {
             var existingContractTransactionReceiptXref = _db.ContractTransactionReceiptXrefs.FirstOrDefault(x => x.ContractTransactionReceiptXrefID == Xref.ContractTransactionReceiptXrefID);

             if (existingContractTransactionReceiptXref != null)
             {
                 existingContractTransactionReceiptXref.ContractTransactionReceiptXrefID = Xref.ContractTransactionReceiptXrefID;
                 existingContractTransactionReceiptXref.ContractTransactionID = Xref.ContractTransactionID;
                 existingContractTransactionReceiptXref.ReceiptID = Xref.ReceiptID;
                 existingContractTransactionReceiptXref.CreateDate = Xref.CreateDate;
                 existingContractTransactionReceiptXref.IsActive = Xref.IsActive;

                 return _db.SaveChanges();
             }

             return 0;
         }

         public void Dispose()
         {
             ((IDisposable)_db).Dispose();
         }
    }
}
