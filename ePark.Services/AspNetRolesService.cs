﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;

namespace ePark.Services
{
    public class AspNetRolesService : IDisposable
    {
        
         private readonly EparkContext _db;

         public AspNetRolesService()
        {
            _db = new EparkContext();
        }

         public AspNetRole GetById(int id)
         {
             // if not found will return null
             return _db.AspNetRoles.FirstOrDefault(x => x.Id == id);
         }
        
         public List<AspNetRole> GetList()
         {
             return _db.AspNetRoles.ToList();
         }

        
          public void Dispose()
        {
            ((IDisposable)_db).Dispose();
        }

    }
}
