﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;

namespace ePark.Services
{
    public class vwReceiptsServices : IDisposable
    {
         private readonly EparkContext _db;
         public vwReceiptsServices()
        {
            _db = new EparkContext();
        }

         public List<vwReceipt> GetList()
         {
             // if not found will return null
             return _db.vwReceipts.ToList();
         }

         public List<vwReceipt> GetListByReceiptId(int id)
         {
             // if not found will return null
             List<vwReceipt> receipts = _db.vwReceipts.Where(x => x.ReceiptID == id).ToList();
             return receipts;
            // return _db.vwReceipts.Where(x => x.ReceiptID == id).ToList();
         }



        public void Dispose()
        {
            ((IDisposable)_db).Dispose();
        }
    }
}
