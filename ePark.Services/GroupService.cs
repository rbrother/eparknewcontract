﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;


namespace ePark.Services 
{
    public class GroupService : IDisposable
    {
         private readonly EparkContext _db;
         public GroupService()
        {
            _db = new EparkContext();
        }

         public Group GetById(int GroupID)
         {
             // if not found will return null
             return _db.Groups.FirstOrDefault(x => x.GroupID == GroupID);
         }
          public int Create(Group group)
         {
             var newGroup = new Group
             {
                GroupCategoryID = group.GroupCategoryID,
                GroupDesc = group.GroupDesc,
                IsActive = group.IsActive
             };

             _db.Groups.Add(newGroup);

             return _db.SaveChanges();
         }
         public List<Group> GetList()
         {
             // if not found will return null
             return _db.Groups.ToList();
         }

         public int Update(Group group)
         {
             var existingGroup = _db.Groups.FirstOrDefault(x => x.GroupID == group.GroupID);

             if (existingGroup != null)
             {
                  existingGroup.GroupID = group.GroupID;
                  existingGroup.GroupCategoryID = group.GroupCategoryID;
                  existingGroup.GroupDesc = group.GroupDesc;
                  existingGroup.IsActive = group.IsActive;

                 return _db.SaveChanges();
             }

             return 0;
         }

         public int Delete(Group group)
         {
             var existingGroup = _db.Groups.FirstOrDefault(x => x.GroupID == group.GroupID);

             if (existingGroup != null)
             {
                 _db.Groups.Remove(group);
                 return _db.SaveChanges();
             }

             return 0;
         }

         public int DeleteById(int groupID)
         {
             var existingGroup = _db.Groups.FirstOrDefault(x => x.GroupID == groupID);

             if (existingGroup != null)
             {
                 _db.Groups.Remove(existingGroup);
                 return _db.SaveChanges();
             }

             return 0;
         }

       


        public void Dispose()
        {
            ((IDisposable)_db).Dispose();
        }

    }
}
