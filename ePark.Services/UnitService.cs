﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;

namespace ePark.Services
{
    public class UnitService : IDisposable
    {
        private readonly EparkContext _db;
        public UnitService()
        {
            _db = new EparkContext();
        }

        public Unit GetById(int unitId)
        {
            // if not found will return null
            return _db.Units.FirstOrDefault(x => x.UnitID == unitId);
        }

        public List<Unit> GetList()
        {
            return _db.Units.ToList();
        }

        public int Create(Unit unit)
        {
            var newUnit = new Unit
            {
                UnitID = unit.UnitID,
                UnitDesc = unit.UnitDesc,
                CreatedDate = unit.CreatedDate,
                IsAvailable = unit.IsAvailable,
                IsActive = unit.IsActive
            };

            _db.Units.Add(newUnit);

            return _db.SaveChanges();
        }

        public int Update(Unit unit)
        {
            var existingUnit = _db.Units.FirstOrDefault(x => x.UnitID == unit.UnitID);

            if (existingUnit != null)
            {
                existingUnit.UnitID = unit.UnitID;
                existingUnit.UnitDesc = unit.UnitDesc;
                existingUnit.CreatedDate = unit.CreatedDate;
                existingUnit.IsAvailable = unit.IsAvailable;
                existingUnit.IsActive = unit.IsActive;

                return _db.SaveChanges();
            }

            return 0;
        }

        public int Delete(Unit unit)
        {
            var existingUnit = _db.Units.FirstOrDefault(x => x.UnitID == unit.UnitID);

            if (existingUnit != null)
            {
                _db.Units.Remove(existingUnit);
                return _db.SaveChanges();
            }

            return 0;
        }

        public int DeleteById(int unitId)
        {
            var existingUnit = _db.Units.FirstOrDefault(x => x.UnitID == unitId);

            if (existingUnit != null)
            {
                _db.Units.Remove(existingUnit);
                return _db.SaveChanges();
            }

            return 0;
        }

        public void Dispose()
        {
            ((IDisposable)_db).Dispose();
        }
    }
}
