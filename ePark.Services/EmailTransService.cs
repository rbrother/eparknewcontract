﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;

namespace ePark.Services
    {

    public class EmailTransService : IDisposable
    {
         private readonly EparkContext _db;

        public EmailTransService()
        {
            _db = new EparkContext();
        }

        public EmailTran GetById(int emailTranID)
        {
            // if not found will return null
            return _db.EmailTrans.FirstOrDefault(x => x.EmailTransID == emailTranID);
        }

        public List<EmailTran> GetList()
        {
            return _db.EmailTrans.ToList();
        }
      
        public int Create(EmailTran emailTran)
        {
            var newEmailTran = new EmailTran
            {
                EmailTransID = emailTran.EmailTransID,
                EmailTemplateID = emailTran.EmailTemplateID,
                ParkingContractReservationID = emailTran.ParkingContractReservationID,
                EmailStatusID = emailTran.EmailStatusID,
                AspNetUsersID = emailTran.AspNetUsersID,
                transLogID = emailTran.transLogID,
                CreateDate = emailTran.CreateDate,
                Comments = emailTran.Comments,
                WillSendDate = emailTran.WillSendDate,
                SentDate = emailTran.SentDate
            };

            _db.EmailTrans.Add(newEmailTran);

            return _db.SaveChanges();
        }

        public int Update(EmailTran emailTran)
        {
            var existingEmailTran = _db.EmailTrans.FirstOrDefault(x => x.EmailTransID == emailTran.EmailTransID);

            if (existingEmailTran != null)
             {
                 existingEmailTran.EmailTransID = emailTran.EmailTransID;
                 existingEmailTran.EmailTemplateID = emailTran.EmailTemplateID;
                 existingEmailTran.ParkingContractReservationID = emailTran.ParkingContractReservationID;
                 existingEmailTran.EmailStatusID = emailTran.EmailStatusID;
                 existingEmailTran.AspNetUsersID = emailTran.AspNetUsersID;
                 existingEmailTran.transLogID = emailTran.transLogID;
                 existingEmailTran.CreateDate = emailTran.CreateDate;
                 existingEmailTran.Comments = emailTran.Comments;
                 existingEmailTran.WillSendDate = emailTran.WillSendDate;
                 existingEmailTran.SentDate = emailTran.SentDate;

                return _db.SaveChanges();
              }

              return 0;
          }

        public int Delete(EmailTran emailTran)
        {
            var existingEmailTran = _db.EmailTrans.FirstOrDefault(x => x.EmailTransID == emailTran.EmailTransID);

            if (existingEmailTran != null)
            {
                _db.EmailTrans.Remove(existingEmailTran);
                return _db.SaveChanges();
            }

            return 0;
        }

        public int DeleteById(int emailTransID)
        {
            var existingEmailTran = _db.EmailTrans.FirstOrDefault(x => x.EmailTransID == emailTransID);

            if (existingEmailTran != null)
            {
                _db.EmailTrans.Remove(existingEmailTran);
                return _db.SaveChanges();
            }

            return 0;
        }

        public void Dispose()
        {
            ((IDisposable)_db).Dispose();
        }

    }
}
