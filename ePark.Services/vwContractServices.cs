﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;

namespace ePark.Services
{
    public class vwContractServices : IDisposable
    {
         private readonly EparkContext _db;
         public vwContractServices()
        {
            _db = new EparkContext();
        }
        
         public List<vwContract> GetList()
         {
             // if not found will return null
             return _db.vwContracts.ToList();
         }

         public List<vwContract> GetListByUserId(int id)
         {
             // if not found will return null
             return _db.vwContracts.Where(x => x.AspNetUsersId == id).ToList();
         }

                 public void Dispose()
         {
             ((IDisposable)_db).Dispose();
         }

    }
}
