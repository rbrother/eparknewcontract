﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;

namespace ePark.Services
{
    public class EmployerService : IDisposable
    {
        private readonly EparkContext _db;
        public EmployerService()
        {
            _db = new EparkContext();
        }

        public List<Employer> GetByAspNetUserId(int aspnetUserId)
        {
            return _db.Employers.Where(x => x.AspNetUserID == aspnetUserId).ToList();
        }

        public Employer GetById(int employerId)
        {
            return _db.Employers.FirstOrDefault(x => x.EmployerID == employerId);
        }

        public List<Employer> GetList()
        {
            return _db.Employers.ToList();
        }

        public int Create(Employer employer)
        {
            var newEmployer = new Employer
            {
                AspNetUserID = employer.AspNetUserID,
                BusinessName = employer.BusinessName,
                Address1 = employer.Address1,
                Address2 = employer.Address2,
                City = employer.City,
                State = employer.State,
                PostalCode = employer.PostalCode
            };

            _db.Employers.Add(employer);

            return _db.SaveChanges();
        }

        public int Update(Employer employer)
        {
            var existingEmployer = _db.Employers.FirstOrDefault(x => x.EmployerID == employer.EmployerID);

            if (existingEmployer != null)
            {
                existingEmployer.AspNetUserID = employer.AspNetUserID;
                existingEmployer.BusinessName = employer.BusinessName;
                existingEmployer.Address1 = employer.Address1;
                existingEmployer.Address2 = employer.Address2;
                existingEmployer.City = employer.City;
                existingEmployer.State = employer.State;
                existingEmployer.PostalCode = employer.PostalCode;

                return _db.SaveChanges();
            }
            return 0;
        }

        public void Dispose()
        {
            ((IDisposable)_db).Dispose();
        }
    }
}
