﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;

namespace ePark.Services
{
    public class AspNetUserProfileRolesViewService :IDisposable
    {
        private readonly EparkContext _db;
        public AspNetUserProfileRolesViewService()
        {
            _db = new EparkContext();
        }

        public vwAspNetUserProfileRole GetByUserId(int userId)
        {
            // if not found will return null
            return _db.vwAspNetUserProfileRoles.FirstOrDefault(x => x.AspNetUsersId == userId);
        }

        public List<vwAspNetUserProfileRole> GetList()
        {
            // if not found will return null
            return _db.vwAspNetUserProfileRoles.ToList();
        }


         public void Dispose()
        {
            ((IDisposable)_db).Dispose();
        }
    }
}
