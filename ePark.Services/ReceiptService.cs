﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;

namespace ePark.Services
{
    public class ReceiptService : IDisposable
    {
        
         private readonly EparkContext _db;
         public ReceiptService()
        {
            _db = new EparkContext();
        }


         public Receipt GetById(int ReceiptID)
         {
             // if not found will return null
             return _db.Receipts.FirstOrDefault(x => x.ReceiptID == ReceiptID);
         }

         public int Create(Receipt receipt)
         {
             var newReceipt = new Receipt
             {
                 ReceiptPayStatusID = receipt.ReceiptPayStatusID,
                 ReceiptPayTypeID = receipt.ReceiptPayTypeID,
                 BillingCardTypesID = receipt.BillingCardTypesID,
                 PayDate = receipt.PayDate,
                 ReceiptNumber = receipt.ReceiptNumber,
                 ReceiptTotalAmount = receipt.ReceiptTotalAmount,
                 ConvFee = receipt.ConvFee,
                 Tax = receipt.Tax,
                 CBIDFee = receipt.CBIDFee
             };

             _db.Receipts.Add(newReceipt);
             int numrows = _db.SaveChanges();

             return newReceipt.ReceiptID;
         }
         public List<Receipt> GetList()
         {
             // if not found will return null
             return _db.Receipts.ToList();
         }

        public List<ReceiptPayType> GetPayTypeList()
        {

            return _db.ReceiptPayTypes.ToList();
        }


        public ReceiptPayType GetPayTypeById(int PayTypeId)
        {

            return _db.ReceiptPayTypes.FirstOrDefault(x => x.ReceiptPayTypeID == PayTypeId);
        }



        public List<ReceiptPayStatu> GetPayStatusList()
        {
            return _db.ReceiptPayStatus.ToList();
        }


        public List<BillingCardType> GetBillingCardList()
        {
            return _db.BillingCardTypes.ToList();
        }


        public BillingCardType GetBillingCardTypeById(int TypeID)
        {
            return _db.BillingCardTypes.FirstOrDefault(x => x.BillingCardTypesID == TypeID);
        }

        public string getPaymentMtehod(Receipt receipt)
        {
            return
                _db.ReceiptPayTypes.FirstOrDefault(x => x.ReceiptPayTypeID == receipt.ReceiptPayTypeID)
                    .ReceiptPayTypeDesc;

        }

         public int Update(Receipt receipt)
         {
             var existingReceipt = _db.Receipts.FirstOrDefault(x => x.ReceiptID == receipt.ReceiptID);

             if (existingReceipt != null)
             {
                 existingReceipt.ReceiptID = receipt.ReceiptID;
                 existingReceipt.ReceiptNumber = receipt.ReceiptNumber;
                // existingReceipt.ReceiptPayStatu = receipt.ReceiptPayStatu;
                 existingReceipt.ReceiptPayStatusID = receipt.ReceiptPayStatusID;
                // existingReceipt.ReceiptPayType = receipt.ReceiptPayType;
                 existingReceipt.ReceiptPayTypeID = receipt.ReceiptPayTypeID;
                 existingReceipt.ReceiptTotalAmount = receipt.ReceiptTotalAmount;
                 existingReceipt.Tax = receipt.Tax;
                 existingReceipt.BillingCardTypesID = receipt.BillingCardTypesID;
                 existingReceipt.CBIDFee = receipt.CBIDFee;
              //   existingReceipt.ContractTransactionReceiptXrefs = receipt.ContractTransactionReceiptXrefs;
                 existingReceipt.ConvFee = receipt.ConvFee;
                 existingReceipt.PayDate = receipt.PayDate;

                 return _db.SaveChanges();
             }

             return 0;
         }

         public int Delete(Group group)
         {
             var existingGroup = _db.Groups.FirstOrDefault(x => x.GroupID == group.GroupID);

             if (existingGroup != null)
             {
                 _db.Groups.Remove(group);
                 return _db.SaveChanges();
             }

             return 0;
         }

         public int DeleteById(int groupID)
         {
             var existingGroup = _db.Groups.FirstOrDefault(x => x.GroupID == groupID);

             if (existingGroup != null)
             {
                 _db.Groups.Remove(existingGroup);
                 return _db.SaveChanges();
             }

             return 0;
         }
        public void Dispose()
        {
            ((IDisposable)_db).Dispose();
        }
    }
}
