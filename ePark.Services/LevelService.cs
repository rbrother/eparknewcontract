﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;

namespace ePark.Services
{
   public class LevelService : IDisposable
    {
       private readonly EparkContext _db;
        public LevelService()
        {
            _db = new EparkContext();
        }

        public Level GetById(int levelId)
        {
            // if not found will return null
            return _db.Levels.FirstOrDefault(x => x.LevelID == levelId);
        }

        public List<Level> GetList()
        {
            // if not found will return null
            return _db.Levels.ToList();
        }

        public int Create(Level level)
        {
            var newLevel = new Level
            {
                VenueID = level.VenueID,
                LevelDesc = level.LevelDesc,
                CreateDate = level.CreateDate,
                IsActive = level.IsActive
            };

            _db.Levels.Add(newLevel);

            return _db.SaveChanges();
        }

        public int Update(Level level)
        {
            var existingLevel = _db.Levels.FirstOrDefault(x => x.LevelID == level.LevelID);

            if (existingLevel != null)
            {
                existingLevel.VenueID = level.VenueID;
                existingLevel.LevelDesc = level.LevelDesc;
                existingLevel.CreateDate = level.CreateDate;
                existingLevel.IsActive = level.IsActive;

                return _db.SaveChanges();
            }

            return 0;
        }

        public int Delete(Level level)
        {
            var existingLevel = _db.Levels.FirstOrDefault(x => x.LevelID == level.LevelID);

            if (existingLevel != null)
            {
                _db.Levels.Remove(existingLevel);
                return _db.SaveChanges();
            }

            return 0;
        }

        public int DeleteById(int levelId)
        {
            var existingLevel = _db.Levels.FirstOrDefault(x => x.LevelID == levelId);

            if (existingLevel != null)
            {
                _db.Levels.Remove(existingLevel);
                return _db.SaveChanges();
            }

            return 0;
        }


       public void Dispose()
       {
           ((IDisposable)_db).Dispose();
       }
    }
}
