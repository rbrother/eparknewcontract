﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;
namespace ePark.Managers
{
    public class vwContractManager
    {

        public List<vwContract> GetList()
        {
            using (var contractService = new vwContractServices())
            {
                return contractService.GetList();
            }
        }

        public List<vwContract> GetListByUserId(int userId)
        {
            using (var contractService = new vwContractServices())
            {
                return contractService.GetListByUserId(userId);
            }
        }

    }
}
