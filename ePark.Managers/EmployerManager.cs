﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;

namespace ePark.Managers
{
    public class EmployerManager
    {
        public Employer GetById(int employerId)
        {
            using (var employerService = new EmployerService())
            {
                return employerService.GetById(employerId);
            }
        }

        public List<Employer> GetList()
        {
            using (var employerService = new EmployerService())
            {
                return employerService.GetList();
            }
        }

        public List<Employer> GetByGetByAspNetUserId(int aspnetUserId)
        {
            using (var employerService = new EmployerService())
            {
                return employerService.GetByAspNetUserId(aspnetUserId);
            }
        }

        public int Create(Employer parkingEmployer)
        {
            using (var employerService = new EmployerService())
            {
                return employerService.Create(parkingEmployer);
            }
        }

        public int Update(Employer parkingEmployer)
        {
            using (var employerService = new EmployerService())
            {
                return employerService.Update(parkingEmployer);
            }
        }
    }
}
