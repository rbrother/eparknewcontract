﻿using System.Collections.Generic;
using ePark.Data;
using ePark.Services;

namespace ePark.Managers
{
    public class vwReceiptsManager
    {
        
        public List<vwReceipt> GetList()
        {
            using (var receiptService = new vwReceiptsServices())
            {
                return receiptService.GetList();
            }
        }

        public List<vwReceipt> GetListByReceiptId(int receiptid)
        {
            using (var receiptService = new vwReceiptsServices())
            {
                return receiptService.GetListByReceiptId(receiptid);
            }
        }

    }
}
