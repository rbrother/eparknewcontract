﻿using System.Collections.Generic;
using ePark.Data;
using ePark.Services;
namespace ePark.Managers
{
    public class vwPaymentReceiptsManager
    {
        
        public List<vwPaymentReceipt> GetList()
        {
            using (var receiptService = new vwPaymentReceiptsService())
            {
                return receiptService.GetList();
            }
        }

        public List<vwPaymentReceipt> GetListByReceiptId(int receiptid)
        {
            using (var receiptService = new vwPaymentReceiptsService())
            {
                return receiptService.GetListByReceiptId(receiptid);
            }
        }

    }
}
