﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;

namespace ePark.Managers
{
    public class VehicleManager
    {

        public Vehicle GetById(int vehicleId)
        {
            using (var vehicleService = new VehicleService())
            {
                return vehicleService.GetById(vehicleId);
            }
        }

        public List<Vehicle> GetList()
        {
            using (var vehicleService = new VehicleService())
            {
                return vehicleService.GetList();
            }
        }


        public List<Vehicle> GetByContract(int contractId)
        {
            using (var vehicleService = new VehicleService())
            {
                return vehicleService.GetByContract(contractId);
            }
        }


        public List<Vehicle> get()
        {
            using (var vehicleService = new VehicleService())
            {
                return vehicleService.GetList();
            }
        }

        public int Create(Vehicle parkingVehicle)
        {
            using (var vehicleService = new VehicleService())
            {
                return vehicleService.Create(parkingVehicle);
            }
        }

        public int Update(Vehicle parkingVehicle)
        {
            using (var vehicleService = new VehicleService())
            {
                return vehicleService.Update(parkingVehicle);
            }
        }
    }
}
