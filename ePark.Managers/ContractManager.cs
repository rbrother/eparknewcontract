﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;

namespace ePark.Managers
{
    public class ContractManager
    {
        public Contract GetById(int contractId)
        {
            using (var contractService = new ContractService())
            {
                return contractService.GetById(contractId);
            }
        }

        public List<Contract> GetList()
        {
            using (var contractService = new ContractService())
            {
                return contractService.GetList();
            }
        }

        //another comment
        public List<Contract> GetListWithTransactionsByAspNetUserId(int id)
        {
            using (var contractService = new ContractService())
            {
                return contractService.GetListWithTransactionsByUserId(id);
            }
        }

        public Contract GetByAspNetUserId(int id)
        {
            using (var contractService = new ContractService())
            {
                return contractService.GetByUserId(id);
            }
        }
        
        
        public int Create(Contract contract)
        {
            using (var contractService = new ContractService())
            {
                return contractService.Create(contract);
            }
        }

        public int Update(Contract contract)
        {
            using (var contractService = new ContractService())
            {
                return contractService.Update(contract);
            }
        }
    }
}
