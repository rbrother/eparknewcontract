﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;

namespace ePark.Managers
{
    public class ContractDocumentManager
    {
        public ContractDocument GetById(int documentContractId)
        {
            using (var contractDocumentService = new ContractDocumentService())
            {
                return contractDocumentService.GetById(documentContractId);
            }
        }

        public ContractDocument GetByFileName(string filename)
        {
            using (var contractDocumentService = new ContractDocumentService())
            {
                return contractDocumentService.GetByFileName(filename);
            }
        }

        public List<ContractDocument> GetByContractId(int contractid)
        {
            using (var contractDocumentService = new ContractDocumentService())
            {
                return contractDocumentService.GetByContractId(contractid);
            }
        }

        public List<ContractDocument> GetList()
        {
            using (var contractDocumentService = new ContractDocumentService())
            {
                return contractDocumentService.GetList();
            }
        }
        public int Create(ContractDocument contract)
        {
            using (var contractDocumentService = new ContractDocumentService())
            {
                return contractDocumentService.Create(contract);
            }
        }

        public int Update(ContractDocument parkingDocumentContract)
        {
            using (var contractDocumentService = new ContractDocumentService())
            {
                return contractDocumentService.Update(parkingDocumentContract);
            }
        }

        public int Delete(ContractDocument contractDocument)
        {
            using (var contractDocumentService = new ContractDocumentService())
            {
                return contractDocumentService.Delete(contractDocument);
            }
        }

       
    }
}
