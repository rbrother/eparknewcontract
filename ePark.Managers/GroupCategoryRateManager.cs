﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;

namespace ePark.Managers
{
    public class GroupCategoryRateManager
    {
        public List<vwGroupCategoryRate> GetGroupByCategoryId(int parkingGroupCategoryID)
        {
            using (var groupCategoryRateService = new GroupCategoryRateService())
            {
                return groupCategoryRateService.GetByCategoryId(parkingGroupCategoryID);
            }
        }

        public List<vwGroupCategoryRate> GetList()
        {
            using (var groupCategoryRateService = new GroupCategoryRateService())
            {
                return groupCategoryRateService.GetList();
            }
        }

        public vwGroupCategoryRate GetByGroupId(int parkingGroupID)
        {
            using (var groupCategoryRateService = new GroupCategoryRateService())
            {
                return groupCategoryRateService.GetByGroupId(parkingGroupID);
            }
        }
       
    }
}
