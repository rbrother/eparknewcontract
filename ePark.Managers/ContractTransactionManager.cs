﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;

namespace ePark.Managers
{
    public class ContractTransactionManager
    {
        public ContractTransaction GetById(int transactionId)
        {
            using (var contractTransactionService = new ContractTransactionService())
            {
                return contractTransactionService.GetById(transactionId);
            }
        }

        public ContractTransaction GetDepositforContract(int ContractId)
        {
            using (var contractTransactionService = new ContractTransactionService())
            {
                return contractTransactionService.GetDepositforContract(ContractId);
            }
        }
        
        public ContractTransaction GetPaymentforContract(int ContractId)
        {
            using (var contractTransactionService = new ContractTransactionService())
            {
                return contractTransactionService.GetPaymentforContract(ContractId);
            }
        }

        public List<ContractTransaction> GetList()
        {
            using (var contractTransactionService = new ContractTransactionService())
            {
                return contractTransactionService.GetList();
            }
        }
        public int Create(ContractTransaction contract)
        {
            using (var contractTransactionService = new ContractTransactionService())
            {
                return contractTransactionService.Create(contract);
            }
        }

        public int Update(ContractTransaction contract)
        {
            using (var contractTransactionService = new ContractTransactionService())
            {
                return contractTransactionService.Update(contract);
            }
        }

        public int Delete(ContractTransaction contractTransaction)
        {
            using (var contractTransactionService = new ContractTransactionService())
            {
                return contractTransactionService.Delete(contractTransaction);
            }
        }

        public int DeleteById(int transactionId)
        {
            using (var contractTransactionService = new ContractTransactionService())
            {
                return contractTransactionService.DeleteById(transactionId);
            }
        }


    }
}
