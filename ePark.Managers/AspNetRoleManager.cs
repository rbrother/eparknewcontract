﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;

namespace ePark.Managers
{
    public class AspNetRoleManager
    {

        public AspNetRole GetById(int aspNetRoleId)
        {
            using (var aspNetRolesService = new AspNetRolesService())
            {
                return aspNetRolesService.GetById(aspNetRoleId);
            }
        }

        public List<AspNetRole> GetList()
        {
            using (var aspNetRoleService = new AspNetRolesService())
            {
                return aspNetRoleService.GetList();
            }
        }

    }
}
