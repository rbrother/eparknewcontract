﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ePark.Data;
using ePark.Services;

namespace ePark.Managers
{
    public class EmailTemplateManager
    {
        public EmailTemplate GetById(int emailTempId)
        {
            using (var emailTempService = new EmailTemplateService())
            {
                return emailTempService.GetById(emailTempId);
            }
        }

        public List<EmailTemplate> GetList()
        {
            using (var emailTempService = new EmailTemplateService())
            {
                return emailTempService.GetList();
            }
        }
        public int Create(EmailTemplate parkingInquiry)
        {
            using (var emailTempService = new EmailTemplateService())
            {
                return emailTempService.Create(parkingInquiry);
            }
        }

        public int Update(EmailTemplate EmailTempID)
        {
            using (var emailTempService = new EmailTemplateService())
            {
                return emailTempService.Update(EmailTempID);
            }
        }



    }
}
