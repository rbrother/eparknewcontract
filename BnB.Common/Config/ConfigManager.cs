using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BnB.Common.Config
{
    class ConfigManager
    {
        public static string SiteDisplayName()
        {
            return ConfigurationManager.AppSettings["SiteDisplayName"];
        }

        public static string GetSiteBaseUrl()
        {
            return ConfigurationManager.AppSettings["SiteBaseUrl"];
        }

        public static string SiteUrlDisplayName()
        {
            return ConfigurationManager.AppSettings["SiteUrlDisplayName"];
        }

        public static string SiteEmailAddress()
        {
            return ConfigurationManager.AppSettings["SiteEmailAddress"];
        }

        public static string EnforceSSL()
        {
            return ConfigurationManager.AppSettings["EnforceSSL"];
        }

        public static string GetSmtpHost()
        {
            return ConfigurationManager.AppSettings["SmtpHost"];
        }

        public static string GetSmtpPort()
        {
            return ConfigurationManager.AppSettings["SmtpPort"];
        }

        public static string GetSmtpUserName()
        {
            return ConfigurationManager.AppSettings["SmtpUserName"];
        }

        public static string GetSmtpUserDisplayName()
        {
            return ConfigurationManager.AppSettings["SmtpUserDisplayName"];
        }

        public static string GetSmtpPassword()
        {
            return ConfigurationManager.AppSettings["SmtpPassword"];
        }
        public static string GetDebugMailRecepient1()
        {
            return ConfigurationManager.AppSettings["DebugMailRecepient1"];
        }
        public static string GetDebugMailRecepient2()
        {
            return ConfigurationManager.AppSettings["DebugMailRecepient2"];
        }
    }
}
