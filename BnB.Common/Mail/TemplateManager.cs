﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using BnB.Common.Config;
using BnB.Common.Templates;

namespace BnB.Common.Mail
{
    internal class TemplateManager
    {
        private readonly List<KeyValuePair<string, string>> _emailDataInserts;

        public TemplateManager(List<KeyValuePair<string, string>> emailDataInserts)
        {
            _emailDataInserts = emailDataInserts;
        }
        internal MailMessage CreateMailMessage(string emailType)
        {
            var emailTemplate = GetTemplate(emailType);

            var mailMessage = new MailMessage
            {
                From = new MailAddress(ConfigManager.GetSmtpUserName(), ConfigManager.GetSmtpUserDisplayName()),
                Body = emailTemplate,
                IsBodyHtml = true
            };

            mailMessage.Bcc.Add(ConfigManager.GetDebugMailRecepient1());
            mailMessage.Bcc.Add(ConfigManager.GetDebugMailRecepient2());

            return mailMessage;
        }
        private string GetTemplate(string emailType)
        {
            string emailMessageBody;

            using (var streamReader = new StreamReader(String.Format("{0}{1}{2}", AppDomain.CurrentDomain.BaseDirectory, "/Email/Templates/", emailType)))
            {
                emailMessageBody = streamReader.ReadToEnd();
            }

            return InjectCommonDynamicFields(emailMessageBody);
        }

        private string InjectCommonDynamicFields(string messageBody)
        {
            var messageOut = new StringBuilder(messageBody);

            _emailDataInserts.Add(new KeyValuePair<string, string>("<%SiteDisplayName%>", ConfigManager.SiteDisplayName()));
            _emailDataInserts.Add(new KeyValuePair<string, string>("<%FullSiteUrl%>", UrlManager.FullSiteUrl()));
            _emailDataInserts.Add(new KeyValuePair<string, string>("<%SiteUrlDisplayName%>", ConfigManager.SiteUrlDisplayName()));
            _emailDataInserts.Add(new KeyValuePair<string, string>("<%SiteEmailAddress%>", ConfigManager.SiteEmailAddress()));

            foreach (var emailDataInsert in _emailDataInserts)
            {
                messageOut.Replace(emailDataInsert.Key, emailDataInsert.Value);
            }

            return messageOut.ToString();
        }
    }
}
