using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using BnB.Common.Config;
using BnB.Common.Templates;
using ePark.Data;
using ePark.Managers;

namespace BnB.Common.Mail
{
    public class MailManager
    {

        protected static EmailTran EmailTranPoCo(EmailTemplate emailtemplate, int userid, string comments, int contractid)
        {
            EmailTran postmasteremailtran = new EmailTran();

            postmasteremailtran.CreateDate = System.DateTime.Now;
            postmasteremailtran.EmailStatusID = 1;
            postmasteremailtran.EmailTemplateID = emailtemplate.EmailTemplateID;
            postmasteremailtran.SentDate = System.DateTime.Now;
            postmasteremailtran.AspNetUsersID = userid;
            postmasteremailtran.Comments = comments;
            postmasteremailtran.ParkingContractReservationID = contractid;
            return postmasteremailtran;
        }

        protected static SmtpClient MrPostMan()
        {
            var smtpClient = new SmtpClient
            {
                Host = ConfigManager.GetSmtpHost(),
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(ConfigManager.GetSmtpUserName(), ConfigManager.GetSmtpPassword()),
                Port = Convert.ToInt16(ConfigManager.GetSmtpPort())
            };

            return smtpClient;
        }

        public static SmtpClient CreateSmtpClient()
        {
            var smtpClient = new SmtpClient
            {
                Host = ConfigManager.GetSmtpHost(),
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(ConfigManager.GetSmtpUserName(), ConfigManager.GetSmtpPassword()),
                Port = Convert.ToInt16(ConfigManager.GetSmtpPort())
            };

            return smtpClient;
        }

        protected static bool SendMessage(MailMessage mailMessage)
        {
            try
            {
                using (var mailMan = MrPostMan())
                {
                    mailMan.Send(mailMessage);
                    return true;
                }
            }
            catch (Exception ex)
            {
                //log this
                return false;
            }
        }

        public static bool EmailWelcome(int userId)
        {
            var mailsent = false;
            var emailTranMgr = new EmailTransManager();

            var user = getAspNetUser(userId);
            var userprofile = GetAspNetUserProfile((int)user.AspNetUserProfile_Id);
            var contract = getContract(userId);

            string userFullName = String.Format("{0} {1}", userprofile.FirstName, userprofile.LastName);

            EmailTemplate emptemp = populateEmailTemplate(1);
            var emailDataInserts = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("<%UserFullName%>", userFullName),
                new KeyValuePair<string, string>("<%UserId%>", userId.ToString()),
                new KeyValuePair<string, string>("<%SiteBaseUrl%>", UrlManager.FullSiteUrl()),
                new KeyValuePair<string, string>("<%EmailDesc%>", emptemp.EmailShortDesc),
                new KeyValuePair<string, string>("<%EmailText%>", emptemp.EmailText),
                new KeyValuePair<string, string>("<%User%>", userFullName),

            };

            var mailMessage = CreateMailMessage(EmailType.Welcome, emailDataInserts);

            mailMessage.To.Add(new MailAddress(user.Email, String.Format("{0}{1}", userprofile.FirstName, userprofile.LastName)));
            mailMessage.Subject = "Parking Inquiry";

            try
            {
                var emailtran = EmailTranPoCo(emptemp, userId, "Welcome to ePARK email", (int)contract.ContractID);
                emailTranMgr.Create(emailtran);

                mailsent = SendMessage(mailMessage);

                return true;
            }
            catch (Exception ex)
            {

                // PostMaster.SupportErrorReport(LogicDude.GetCurrentUsername(), PostMaster.GetSiteBaseUrl(), ex);

                return mailsent;
            }

            return mailsent;
        }

        public static bool EmailActivationInstructions(int userId, string emailConfirmationToken)
        {
            var mailsent = false;
            var emailTranMgr = new EmailTransManager();

            var user = getAspNetUser(userId);
            var userprofile = GetAspNetUserProfile((int)user.AspNetUserProfile_Id);
            var contract = getContract(userId);

            string userFullName = String.Format("{0} {1}", userprofile.FirstName, userprofile.LastName);

            EmailTemplate emptemp = populateEmailTemplate(2);

            var emailDataInserts = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("<%UserFullName%>", userFullName),
                new KeyValuePair<string, string>("<%EmailConfirmationToken%>", emailConfirmationToken),
                new KeyValuePair<string, string>("<%UserId%>", userId.ToString()),
                new KeyValuePair<string, string>("<%SiteBaseUrl%>", UrlManager.FullSiteUrl()),
                new KeyValuePair<string, string>("<%EmailDesc%>", emptemp.EmailShortDesc),
                new KeyValuePair<string, string>("<%EmailText%>", emptemp.EmailText),
                new KeyValuePair<string, string>("<%User%>", userFullName),
            };

            var mailMessage = CreateMailMessage(EmailType.AccountApproved, emailDataInserts);

            mailMessage.To.Add(new MailAddress(user.Email, String.Format("{0}{1}", userprofile.FirstName, userprofile.LastName)));
            mailMessage.Subject = "Parking Inquiry Approved";

            try
            {
                var emailtran = EmailTranPoCo(emptemp, userId, " ePARK Inquiry has been approved", (int)contract.ContractID);
                emailTranMgr.Create(emailtran);

                mailsent = SendMessage(mailMessage);

                return true;
            }
            catch (Exception ex)
            {

                // PostMaster.SupportErrorReport(LogicDude.GetCurrentUsername(), PostMaster.GetSiteBaseUrl(), ex);

                return mailsent;
            }

            return mailsent;
        }

        public static bool EmailAccountRecovery(int userId, string callbackUrl)
        {
            var mailsent = false;
            var emailTranMgr = new EmailTransManager();

            var user = getAspNetUser(userId);
            var userprofile = GetAspNetUserProfile((int)user.AspNetUserProfile_Id);
            var contract = getContract(userId);

            string userFullName = String.Format("{0} {1}", userprofile.FirstName, userprofile.LastName);

            EmailTemplate emptemp = populateEmailTemplate(1);

            var emailDataInserts = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("<%UserFullName%>", userFullName),
                new KeyValuePair<string, string>("<%UserId%>", userId.ToString()),
                new KeyValuePair<string, string>("<%SiteBaseUrl%>", UrlManager.FullSiteUrl()),
                new KeyValuePair<string, string>("<%EmailDesc%>", emptemp.EmailShortDesc),
                new KeyValuePair<string, string>("<%EmailText%>", emptemp.EmailText),
                new KeyValuePair<string, string>("<%AccountRecoveryUrl%>", callbackUrl),
                new KeyValuePair<string, string>("<%User%>", userFullName),

            };

            var mailMessage = CreateMailMessage(EmailType.AccountRecovery, emailDataInserts);

            mailMessage.To.Add(new MailAddress(user.Email, String.Format("{0}{1}", userprofile.FirstName, userprofile.LastName)));
            mailMessage.Subject = "Parking Inquiry";

            try
            {
                // var emailtran = EmailTranPoCo(emptemp, userId, "Account Recovery Email", (int)contract.ContractID);
                // emailTranMgr.Create(emailtran);

                mailsent = SendMessage(mailMessage);

                return true;
            }
            catch (Exception ex)
            {

                // PostMaster.SupportErrorReport(LogicDude.GetCurrentUsername(), PostMaster.GetSiteBaseUrl(), ex);

                return mailsent;
            }

            return mailsent;
        }

        public static bool EmailPaymentReceipt(int userId, string receiptDate, List<vwPaymentReceipt> resPayments, decimal totalAmount, string userFullName, int contractId, string address1, string city, string state, int postalCode, string phonePrimary)
        {
            var mailsent = false;
            var emailTranMgr = new EmailTransManager();
            EmailTemplate emptemp = populateEmailTemplate(6);


            var receiptRows = new StringBuilder();
            decimal convenienceFeeTotal = 0;
            decimal subtotal = 0;
            decimal taxTotal = 0;

            foreach (var reservationPayment in resPayments)
            {
                receiptRows.Append("<tr><td class=\"width312\" style=\"margin-top: 0;margin-left: 0;margin-right: 0;margin-bottom: 0;padding-top: 14px;padding-bottom: 14px;padding-left: 16px;padding-right: 16px;border-collapse: collapse;border-spacing: 0;-webkit-text-size-adjust: none;font-family: Arial, Helvetica, sans-serif;text-align: left;vertical-align: top;width: 312px;font-size: 14px;line-height: 19px;color: #242424;background-color: #ffffff;\">");
                receiptRows.Append(String.Format("{0}<br />", reservationPayment.GroupDesc));
                receiptRows.Append(String.Format("<span class=\"servDetails\" style=\"font - size: 12px; color: #898989;\">{0}</span></td>", reservationPayment.TransactionDesc));
                receiptRows.Append(String.Format("<td class=\"width84 alignRight\" style=\"margin-top: 0;margin-left: 0;margin-right: 0;margin-bottom: 0;padding-top: 14px;padding-bottom: 14px;padding-left: 16px;padding-right: 16px;border-collapse: collapse;border-spacing: 0;-webkit-text-size-adjust: none;font-family: Arial, Helvetica, sans-serif;text-align: right;vertical-align: top;width: 84px;font-size: 14px;line-height: 19px;color: #242424;background-color: #ffffff;\"><span class=\"desktopHide\" style=\"display: none;font-size: 0;max-height: 0;width: 0;line-height: 0;overflow: hidden;mso-hide: all;\">Quantity: </span>{0}</td>", 1));
                receiptRows.Append(String.Format("<td class=\"width84 alignRight\" style=\"margin-top: 0;margin-left: 0;margin-right: 0;margin-bottom: 0;padding-top: 14px;padding-bottom: 14px;padding-left: 16px;padding-right: 16px;border-collapse: collapse;border-spacing: 0;-webkit-text-size-adjust: none;font-family: Arial, Helvetica, sans-serif;text-align: right;vertical-align: top;width: 84px;font-size: 14px;line-height: 19px;color: #242424;background-color: #ffffff;\"><span class=\"desktopHide\" style=\"display: none;font-size: 0;max-height: 0;width: 0;line-height: 0;overflow: hidden;mso-hide: all;\">Subtotal: </span><span class=\"amount\" style=\"color: #666666;\">{0}</span></td></tr>", reservationPayment.Amount));

                // if (reservationPayment.transUser1 != null) convenienceFeeTotal += Convert.ToInt64(reservationPayment.transUser1);
                if (reservationPayment.Amount != null) subtotal += reservationPayment.Amount.Value;
                //if (reservationPayment.transUser1 != null) taxTotal += Convert.ToDecimal(reservationPayment.transUser1);
            }

            if (resPayments[0].CBIDFee != null) convenienceFeeTotal = (decimal)resPayments[0].CBIDFee.Value;
            if (resPayments[0].Tax != null) taxTotal = (decimal)resPayments[0].Tax.Value;

            var user = getAspNetUser(userId);

            try
            {
                var emailtran = EmailTranPoCo(emptemp, userId, " ePARK Inquiry has been approved", contractId);
                emailTranMgr.Create(emailtran);
                if (user.AspNetUserProfile_Id != null)
                {
                    var userprofile = GetAspNetUserProfile((int)user.AspNetUserProfile_Id);

                    var emailDataInserts = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("<%UserId%>", userId.ToString()),
                    new KeyValuePair<string, string>("<%ReceiptDate%>", receiptDate),
                    new KeyValuePair<string, string>("<%UserFullName%>", userFullName),
                    new KeyValuePair<string, string>("<%Address1%>", address1),
                    new KeyValuePair<string, string>("<%City%>", city),
                    new KeyValuePair<string, string>("<%State%>", state),
                    new KeyValuePair<string, string>("<%PostalCode%>", postalCode.ToString()),
                    new KeyValuePair<string, string>("<%PhonePrimary%>", phonePrimary),
                    new KeyValuePair<string, string>("<%ReceiptRows%>", receiptRows.ToString()),
                    new KeyValuePair<string, string>("<%ContractId%>", contractId.ToString()),
                    new KeyValuePair<string, string>("<%ConvenienceFee%>", String.Format("{0:0,0.00}",convenienceFeeTotal)),
                    new KeyValuePair<string, string>("<%SubTotal%>", String.Format("{0:0,0.00}",subtotal)),
                    new KeyValuePair<string, string>("<%TaxTotal%>", String.Format("{0:0,0.00}",taxTotal)),
                    new KeyValuePair<string, string>("<%Total%>", String.Format("{0:0,0.00}",totalAmount))
                };

                    var mailMessage = CreateMailMessage(EmailType.PaymentReceipt, emailDataInserts);

                    mailMessage.To.Add(new MailAddress(user.Email, String.Format("{0}{1}", userprofile.FirstName, userprofile.LastName)));
                    mailMessage.Subject = "Parking Reservation Receipt";


                    mailsent = SendMessage(mailMessage);

                    return true;
                }
            }
            catch (Exception ex)
            {
                // PostMaster.SupportErrorReport(LogicDude.GetCurrentUsername(), PostMaster.GetSiteBaseUrl(), ex);
                return mailsent;
            }
            // }
            return mailsent;
        }

        public static MailMessage CreateMailMessage(string emailType, List<KeyValuePair<string, string>> emailDataInserts)
        {
            var templateManger = new TemplateManager(emailDataInserts);
            return templateManger.CreateMailMessage(emailType);
        }

        private static EmailTemplate populateEmailTemplate(int templateid)
        {
            var emailtemplate = new EmailTemplateManager();

            return emailtemplate.GetById(templateid);
        }

        private static AspNetUser getAspNetUser(int userid)
        {
            var userMgr = new AspNetUsersManager();

            return userMgr.GetById(userid);

        }

        private static AspNetUserProfile GetAspNetUserProfile(int userid)
        {
            var userprofile = new AspNetUserProfilesManager();

            return userprofile.GetById(userid);
        }

        private static Contract getContract(int userid)
        {
            var contractMgr = new ContractManager();

            return contractMgr.GetByAspNetUserId(userid);

        }
    }
}
