﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Web;

using O2S.Components.PDF4NET;
using O2S.Components.PDF4NET.PDFFile;
using O2S.Components.PDF4NET.Graphics;
using O2S.Components.PDF4NET.Graphics.Shapes;
using O2S.Components.PDF4NET.Forms;
using O2S.Components.PDF4NET.Graphics.Shapes.Transparency;


namespace BnB.Common.PDF4Net
{
    public class TemplateEngine
    {

         public void writeFormField(PDFDocument doc, string fieldName, string fieldValue)
        {


            try
            {
                if (fieldValue != null)
                {
                    if (fieldValue.Trim() != "")
                    {
                        doc.Form.Fields[fieldName].Value = fieldValue;
                        PDFTextBoxField textBoxField = (PDFTextBoxField)doc.Form.Fields[fieldName];
                        //textBoxField.Appearance.VisibilityStatus = FieldVisibilityStatus.Visible;
                    }
                }
            }
            catch (Exception ex)
            {
                string strMsg = ex.Message;
                //No Action
            }

            //try { writeFieldArray(doc, fieldName, fieldValue); }
            //catch
            //{
            //    //No Action
            //}

            //try
            //{
            //    setCheckBoxes(doc, fieldName);
            //}
            //catch
            //{
            //    //No Action
            //}

            //try
            //{
            //    setCheckBoxes(doc, fieldName + "_" + fieldValue);
            //}
            //catch
            //{
            //    //No Action
            //}

            //try
            //{
            //    setSquares(doc, fieldName);
            //}
            //catch
            //{
            //    //No Action
            //}
            //try
            //{
            //    setSquares(doc, fieldName + "_" + fieldValue);
            //}
            //catch
            //{
            //    //No Action
            //}



        }

         public void writeFieldArray(PDFDocument doc, string fieldName, string fieldValue)
        {
            string temp = "#" + fieldValue.Trim() + "#";
            string hold;
            string FieldName;
            string alias = fieldName;

            if (temp.Length > 2)
            {
                for (int i = 1; i < temp.Length; i++)
                {
                    hold = temp.Substring(i, 1);
                    if (hold.Trim() != "#")
                    {
                        FieldName = fieldName + "_(" + i.ToString() + ")";
                        try
                        {
                            doc.Form.Fields[FieldName].Value = hold;
                        }
                        catch
                        {
                            //break;
                        }
                    }
                    else
                    {
                        break;
                    }


                }
            }
        }

         public void writeDateField(PDFDocument doc, string dateFieldName, DateTime dateFieldValue, int i)
        {
            if (dateFieldValue != DateTime.MinValue)
            {
                try
                {
                    formDate fd = new formDate(dateFieldValue.ToString());
                    writeFormField(doc, dateFieldName, fd.date);
                    writeFormField(doc, dateFieldName + "_Month", fd.month);
                    writeFormField(doc, dateFieldName + "_Day", fd.day);
                    writeFormField(doc, dateFieldName + "_Year", fd.year);

                    writeFormField(doc, dateFieldName + "_Month_" + i.ToString(), fd.month);
                    writeFormField(doc, dateFieldName + "_Day_" + i.ToString(), fd.day);
                    writeFormField(doc, dateFieldName + "_Year_" + i.ToString(), fd.year);

                    //writeFormField(doc, dateFieldName + "_1", fd.date);
                    //writeFormField(doc, dateFieldName + "_Month" + "_1", fd.month);
                    //writeFormField(doc, dateFieldName + "_Day" + "_1", fd.day);
                    //writeFormField(doc, dateFieldName + "_Year" + "_1", fd.year);



                }
                catch
                {
                    //No Action
                }
            }
        }

         public void setCheckBoxes(PDFDocument doc, string fieldName)
        {

            try
            {
                setCheckBox(doc, fieldName);
                //fieldName = fieldName;  + "_CheckBox"
                //setCheckBox(doc, fieldName);
                try
                {
                    //doc.Form.Fields[fieldName].Value = "X";
                    //PDFTextBoxField tbf = (PDFTextBoxField)doc.Form.Fields[fieldName];
                    //tbf.Appearance.VisibilityStatus = FieldVisibilityStatus.Visible;
                }
                catch(Exception ex)
                {
                    string strmessaage = ex.Message;
                    //No Action
                }

            }
            catch
            {
                //No Action
            }

        }

         public void setCheckBox(PDFDocument doc, string fieldName)
        {
            int i = 0;
            PDFCheckBoxField multiWidgetCheckBox;

            try
            {
                multiWidgetCheckBox = (PDFCheckBoxField)doc.Form.Fields[fieldName];
                multiWidgetCheckBox.Checked = true;
                
            }
            catch (Exception err)
            {
                string t = err.Message;
                t = err.Message;
                //No Action
            }

        }

         public int calcAge(DateTime dateofbirth)
        {
            TimeSpan ts = DateTime.Now.Subtract(dateofbirth);
            int years = ts.Days / 365;

            return years;

        }

    }

    
    public class formDate
    {
        private string _date;
        private string _day;
        private string _month;
        private string _year;

        public string date
        {
            get { return _date; }
        }

        public string day
        {
            get { return _day; }
        }

        public string month
        {
            get { return _month; }
        }

        public string year
        {
            get { return _year; }
        }

        public formDate(string adt)
        {
            if (adt.Trim().Length > 0)
            {
                DateTime dt = DateTime.Parse(adt);
                _date = dt.ToShortDateString();
                _month = dt.Month.ToString();
                _day = dt.Day.ToString();
                _year = dt.Year.ToString();
                if (_day.Length == 1)
                {
                    _day = "0" + _day;
                }
                if (_month.Length == 1)
                {
                    _month = "0" + _month;
                }

            }
        }





    }


}
