﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ePark.Data;
using ePark.Managers;
using Microsoft.AspNet.Identity;

namespace ePark.WebUI.Areas.Admin.Models
{
    public class PaymentManagementModel
    {
        public int userid { get; set; }

        public string Currentdate { get; set; }

        [DisplayName("Total Amount:   ")]
        public decimal ttlAmount { get; set; }

        [DisplayName("Total CBID Fee:   ")]
        public decimal ttlCBIDFee { get; set; }

        [DisplayName("Total Tax:  ")]
        public decimal ttlTax { get; set; }

        [DisplayName("Amount:   ")]
        [DisplayFormat(DataFormatString = "{0:n2}")]
        public decimal amt { get; set;  }

        [DisplayName("Payment Type:   ")]
        public int SelectedPayTypeId { get; set; }
        public IEnumerable<SelectListItem> PayTypes { get; set; }

        [DisplayName("Card Type:   ")]
        public int BillingCardTypesID { get; set; }
        public IEnumerable<SelectListItem> BillingCardTypes { get; set; }

        [DisplayName("Card Expiration")]
        public int CCMonthSelectedId { get; set; }
        public IEnumerable<SelectListItem> CCMonthItems
        {
            get
            {
                return new[]
            {
                new SelectListItem { Value = "1", Text = "01" },
                new SelectListItem { Value = "2", Text = "02" },
                new SelectListItem { Value = "3", Text = "03" },
                new SelectListItem { Value = "3", Text = "04" },
                new SelectListItem { Value = "3", Text = "05" },
                new SelectListItem { Value = "3", Text = "06" },
                new SelectListItem { Value = "3", Text = "07" },
                new SelectListItem { Value = "3", Text = "08" },
                new SelectListItem { Value = "3", Text = "09" },
                new SelectListItem { Value = "3", Text = "10" },
                new SelectListItem { Value = "3", Text = "11" },
                new SelectListItem { Value = "3", Text = "12" },
            };
            }
        }

        [DisplayName("Card Expiration")]
        public int SelectedCCYearId { get; set; }
        public IEnumerable<SelectListItem> CCYearsItems { get; set; }

        [Required(ErrorMessage = "Credit Card Number is required.")]
        [DisplayName("Credit Card Number:   ")]
        public string CCard { get; set; }

        [DisplayName("Card Validation:   ")]
        [Required(ErrorMessage = "Card Verification Value is required.")]
        public string CVV { get; set; }

        [DisplayName("Card Expiration:   ")]
        public string CCExpMonth { get; set; }

        public string CCExpYear { get; set; }

        [Required(ErrorMessage = "Name on Card Value is required.")]
        [DisplayName("Name on Card:   ")]
        public string CCName { get; set; }

        [Required(ErrorMessage = "Zip Code Value is required.")]
        [DisplayName("Zip:   ")]
        public string CCZip { get; set; }
        
        public string ContractTranstctionIds { get; set; }

        public List<vwReservationPayment> ResPaymentViewList { get; set; }
        public List<vwUserContractTransaction> CurrentPaymentList { get; set; }

        public Receipt receipt
        {
            get;
            set;
        }

        public List<Receipt> ReceiptList
        {
            get; set; }

        public List<ReceiptPayType> ReceiptPayTypeList
        {
            get;
            set;
        }

        public ContractTransaction Contracttransaction
        {
            get;
            set;
        }
        
        public vwReservationPayment ResPaymentView { get; set; }

        public PaymentTransactionLog TranslogLog { get; set; }
        
       
      
    }
}