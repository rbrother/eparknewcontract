﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using ePark.Data;
using System.Web.Mvc;


namespace ePark.WebUI.Areas.Admin.Models
{
    public class UserManagementModel
    {
        public string aspNetUserEmail { get; set; }

        public bool aspnetuserlockedout { get; set; }

        public int userId { get; set; }
        
        [DisplayName("User Role")]
        public int AspNetUserRoleId { get; set; }

        public List<vwAspNetUserProfileRole> Users { get; set; }

        public AspNetUser aspNetUser { get; set; }

        public AspNetUserProfile aspnetuserprofile { get; set; }

        public vwAspNetUserProfileRole User { get; set; }

        //public AspNetUserRole UserRole { get; set; }
        
        public IEnumerable<SelectListItem> aspNetUserRoles { get; set; }

        public AspNetUserRole aspnetuserrole { get; set;  }


    }
} 