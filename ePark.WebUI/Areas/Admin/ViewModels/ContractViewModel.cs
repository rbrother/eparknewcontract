﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ePark.Data;
using ePark.Managers;


namespace ePark.WebUI.Areas.Admin.Models
{
    public class ContractViewModel
    {
        [DisplayName("Name")]
        public string FirstLastName { get; set; }

        public Group Grp { get; set; }

        public Inquiry Inq { get; set; }

        public AspNetUser aspUser { get; set; }

        public vwInquiry InqView { get; set; }

        // ASPNet User

        public int? ASPNetUserId { get; set; }

        [StringLength(256)]
        [DisplayName("Email")]
        public string Email { get; set; }

        [DisplayName("Phone")]
        public string PhoneNumber { get; set; }
        
        [StringLength(256)]
        [DisplayName("User Name")]
        public string UserName { get; set; }
        [DisplayName("Address")]
        public string Address1 { get; set; }
        
        //Inquiry

        public int InquiryID { get; set; }

        public int? InquiryTypeID { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        [DisplayName("Inquiry Created")]
        public DateTime? InquiryCreateDate { get; set; }

        public string Notes { get; set; }
        [DisplayName("Inquiry Approved")]
        public bool IsApproved { get; set; }

        [DisplayName("Approved Date")]
        public DateTime? ApprovedDate { get; set; }

        // Contract
        public int ContractID { get; set; }

        public int? ContractStatusID { get; set; }

        public int? EntityID { get; set; }

        public DateTime? ContractCreateDate { get; set; }

        public bool? ContractIsActive { get; set; }

        // Group
        public int GroupID { get; set; }

        public int? GroupCategoryID { get; set; }

        [DisplayName(" Group")]
        public string GroupDesc { get; set; }

        public bool? GroupIsActive { get; set; }

        // GroupRate
        public int GroupRatesID { get; set; }

        public DateTime? GroupRateBegin { get; set; }

        public DateTime? GroupRateEnd { get; set; }
        [DisplayName("Amount")]
        public decimal? Amount { get; set; }

        public bool? GroupRateIsActive { get; set; }

        [DisplayName("Group")]
        public int SelectedGroupId { get; set; }
        public IEnumerable<SelectListItem> ContractGroups { get; set; }
    }
}