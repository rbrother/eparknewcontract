﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ePark.Data;
using ePark.WebUI.Areas.Admin.Models;
using BnB.Common.Mail;
using ePark.Managers;
using ePark.WebUI.Controllers;
using ePark.WebUI.Helpers;
using ePark.WebUI.Models.Enums;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace ePark.WebUI.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ContractsController : BaseController
    {
        private EparkContext db = new EparkContext();

        // GET: Admin/Contracts
        public ActionResult Index()
        {
            var contracts = db.Contracts.Include(c => c.AspNetUser).Include(c => c.ContractStatu).Include(c => c.Group);
            return View(contracts.ToList());
        }

        // GET: Admin/Contracts
        public ActionResult InquiryList()
        {
            var contracts = db.Contracts.Include(c => c.AspNetUser).Include(c => c.ContractStatu).Include(c => c.Group);
            return View(contracts.ToList());
        }

        // GET: Admin/Contracts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contract contract = db.Contracts.Find(id);
            if (contract == null)
            {
                return HttpNotFound();
            }
            return View(contract);
        }

        // GET: Admin/Contracts/Create
        public ActionResult Create()
        {
            ViewBag.AspNetUserID = new SelectList(db.AspNetUsers, "Id", "Email");
            ViewBag.ContractStatusID = new SelectList(db.ContractStatus, "ContractStatusID", "ContractStatusDesc");
            ViewBag.GroupID = new SelectList(db.Groups, "GroupID", "GroupDesc");
            return View();
        }

        // POST: Admin/Contracts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ContractID,ContractStatusID,AspNetUserID,EntityID,GroupID,CreateDate,IsActive")] Contract contract)
        {
            if (ModelState.IsValid)
            {
                db.Contracts.Add(contract);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AspNetUserID = new SelectList(db.AspNetUsers, "Id", "Email", contract.AspNetUserID);
            ViewBag.ContractStatusID = new SelectList(db.ContractStatus, "ContractStatusID", "ContractStatusDesc", contract.ContractStatusID);
            ViewBag.GroupID = new SelectList(db.Groups, "GroupID", "GroupDesc", contract.GroupID);
            return View(contract);
        }

        // GET: Admin/Contracts/Edit/5
        public ActionResult Edit(int id)
        {

            var contractModel = PopulateInquiry(id);

            return View(contractModel);
            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            //Contract contract = db.Contracts.Find(id);
            //if (contract == null)
            //{
            //    return HttpNotFound();
            //}
            //ViewBag.AspNetUserID = new SelectList(db.AspNetUsers, "Id", "Email", contract.AspNetUserID);
            //ViewBag.ContractStatusID = new SelectList(db.ContractStatus, "ContractStatusID", "ContractStatusDesc", contract.ContractStatusID);
            //ViewBag.GroupID = new SelectList(db.Groups, "GroupID", "GroupDesc", contract.GroupID);
            //return View(contract);
        }

        public ContractViewModel PopulateInquiry(int id)
        {
            var contractModel = new ContractViewModel();
            var groupManager = new GroupManager();
            var inquiryViewMgr = new InquiryViewManager();
            var aspuser = new AspNetUsersManager();

            contractModel.InqView = inquiryViewMgr.GetByInqId(id);

            contractModel.UserName = contractModel.InqView.UserName;
            contractModel.InquiryID = contractModel.InqView.InquiryID;
            contractModel.ContractGroups = GetGroups();
            contractModel.Grp = groupManager.GetById(contractModel.InqView.GroupID.Value);
            contractModel.SelectedGroupId = contractModel.InqView.GroupID.Value;
            contractModel.IsApproved = (bool)contractModel.InqView.IsApproved;
            contractModel.InquiryCreateDate = contractModel.InqView.CreateDate;
            contractModel.Address1 = contractModel.InqView.Address1;
            contractModel.FirstLastName = contractModel.InqView.FirstName + " " + contractModel.InqView.LastName;
            contractModel.Email = contractModel.InqView.Email;
            contractModel.ASPNetUserId = contractModel.InqView.Id;
            contractModel.Amount = contractModel.InqView.Amount;
            return contractModel;
        }

        private IEnumerable<SelectListItem> GetGroups()
        {
            var grouprates = new GroupCategoryRateManager();

            var grplist = grouprates.GetList().Where(gr => gr.GroupRateIsActive == true && gr.GroupRateBegin <= System.DateTime.Now && System.DateTime.Now <= gr.GroupRateEnd)
                .Select(x =>
                                new SelectListItem
                                {
                                    Value = x.GroupID.ToString(),
                                    Text = x.GroupDesc + " $" + string.Format("{0:#.00}", x.Amount)
                                });

            return new SelectList(grplist, "Value", "Text");

        }
        // POST: Admin/Contracts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "ContractID,ContractStatusID,AspNetUserID,EntityID,GroupID,CreateDate,IsActive")] Contract contract)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(contract).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.AspNetUserID = new SelectList(db.AspNetUsers, "Id", "Email", contract.AspNetUserID);
        //    ViewBag.ContractStatusID = new SelectList(db.ContractStatus, "ContractStatusID", "ContractStatusDesc", contract.ContractStatusID);
        //    ViewBag.GroupID = new SelectList(db.Groups, "GroupID", "GroupDesc", contract.GroupID);
        //    return View(contract);
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ContractViewModel model)
        {
            var aspnetUserProfilesManager = new AspNetUserProfilesManager();
            var aspnetUsersManager = new AspNetUsersManager();
            var contractManager = new ContractManager();

            var aspnetUser = aspnetUsersManager.GetById((int)model.ASPNetUserId);
            var aspnetUserProfile = aspnetUserProfilesManager.GetById((int)model.ASPNetUserId);

            var applicationUserManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            string emailConfirmationToken = applicationUserManager.GenerateEmailConfirmationToken(aspnetUserProfile.Id);

            int groupSelected = model.SelectedGroupId;
            int inqID = model.InquiryID;

            model.GroupID = model.SelectedGroupId;

            try
            {
                UpdateInquiry(model);

                if (model.IsApproved)
                {
                    MailManager.EmailActivationInstructions(model.ASPNetUserId.Value, emailConfirmationToken);
                    model.ContractID = contractManager.GetByAspNetUserId(aspnetUser.Id).ContractID;
                    CreateContractTrans(model);
                }
            }
            catch (Exception ex)
            {
                string strMsg = ex.Message.ToString();
                ModelState.AddModelError("", "We encountered an error saving your record.  Please try again.");
                return View(model);
            }

            CreateAlert(AlertType.Success, String.Format("You successfully updated Inquiry #{0}.", model.InquiryID));
            return RedirectToAction("Index", "Dashboard", new { area = "Admin" });
        }

        private void UpdateInquiry(ContractViewModel contractViewModel)
        {
            var inquiryManager = new InquiryManager();
            var inquiry = inquiryManager.GetById(contractViewModel.InquiryID);

            inquiry.GroupID = contractViewModel.SelectedGroupId;
            inquiry.IsApproved = contractViewModel.IsApproved;
            inquiry.ApprovedDate = System.DateTime.Now;

            int result = inquiryManager.Update(inquiry);
        }

        private void CreateContractTrans(ContractViewModel contractViewModel)
        {
            var contractTransactionManager = new ContractTransactionManager();
            var groupCategoryRateManager = new GroupCategoryRateManager();
            var systemParametersManager = new SystemParametersManager();
            var systemParameter = new SystemParameter();

            var contractTransaction = CreateContractTransaction(contractViewModel);
            var vwGroupCategoryRate = groupCategoryRateManager.GetByGroupId(contractViewModel.GroupID);

            try
            {
                contractTransaction.Amount = vwGroupCategoryRate.Amount;
                contractTransaction.GroupID = contractViewModel.GroupID;

                contractTransactionManager.Create(contractTransaction);

                contractTransaction.Amount = systemParametersManager.GetByDesc("Deposit").ParmMoney;
                contractTransaction.ContractTransactionTypeID = 2;

                contractTransactionManager.Create(contractTransaction);

            }
            catch (Exception ex)
            {
                string strMsg = ex.Message.ToString();
                throw;
            }
        }

        private ContractTransaction CreateContractTransaction(ContractViewModel contractViewModel)
        {
            var contracttrans = new ContractTransaction();

            contracttrans.ContractTransactionTypeID = 1;
            contracttrans.CreatedDate = System.DateTime.Now;
            contracttrans.ContractID = contractViewModel.ContractID;
            contracttrans.Amount = contractViewModel.Amount;
            contracttrans.ReservedMonthDateTime = System.DateTime.Now;
            contracttrans.ReservedMonth = System.DateTime.Now.Month.ToString() + "/" +  System.DateTime.Now.Year.ToString();
            contracttrans.Paid = false;

            return contracttrans;
        }

        // GET: Admin/Contracts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contract contract = db.Contracts.Find(id);
            if (contract == null)
            {
                return HttpNotFound();
            }
            return View(contract);
        }

        // POST: Admin/Contracts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Contract contract = db.Contracts.Find(id);
            db.Contracts.Remove(contract);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public JsonResult ProvideInquiries()
        {
            return Json(GetInquiries(), JsonRequestBehavior.AllowGet);
        }

        public Array GetInquiries()
        {
            var vwinquiries = new InquiryViewManager();

            Array rows = vwinquiries.GetList().Select(x => new
            {
                x.Id,
                x.UserName,
                x.FirstName,
                x.LastName,
                x.GroupDesc,
                x.PhonePrimary,
                x.CreateDate,
                x.IsApproved
            }).Where(r => r.IsApproved == false).ToArray();

            return rows;
        }
    }
}
