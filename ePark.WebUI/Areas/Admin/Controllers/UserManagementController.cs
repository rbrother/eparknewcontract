﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ePark.Data;
using ePark.WebUI.Areas.Admin.Models;
using ePark.Managers;
using ePark.WebUI.Controllers;
using ePark.WebUI.Models.Enums;

namespace ePark.WebUI.Areas.Admin.Controllers
{
    public class UserManagementController : BaseController
    {
        [Authorize(Roles = "Admin")]
        // GET: Admin/UserManagement
        public ActionResult Index()
        {
            var userManagmentModel = new UserManagementModel();
            var vwAspNetUsers = new AspNetUserProfileRolesViewManager();
            

            userManagmentModel.Users = vwAspNetUsers.GetList();
            
            return View(userManagmentModel);
        }

        public JsonResult ProvideUsers()
        {
            return Json(GetUsers(), JsonRequestBehavior.AllowGet);
        }

        public Array GetUsers()
        {
            var viewAspNetUserProfileRolesManager = new AspNetUserProfileRolesViewManager();

            Array rows = viewAspNetUserProfileRolesManager.GetList().Select(x => new
            {
                x.AspNetUsersId,
                x.ContractID,
                x.UserName,
                x.FirstName,
                x.LastName,
                x.Email,
                x.Address1,
                x.City,
                x.State,
                x.PhonePrimary,
                x.MccEmployee
            }).ToArray();

            return rows;
        }
        
        public ActionResult UserMaint(int userid)
        {
            var model = new UserManagementModel();
            var vwaspnetuser = new AspNetUserProfileRolesViewManager();
            var aspNetUserMgr = new AspNetUsersManager();
            var aspNetUserProfileMgr = new AspNetUserProfilesManager();
            var aspnetuserrolemgr = new AspNetUserRolesManager();
            //model.Role = new AspNetRole();

            model.User = vwaspnetuser.GetByUserId(userid);
            model.aspNetUser = aspNetUserMgr.GetById(userid);
            model.userId = model.aspNetUser.Id;
            model.aspnetuserlockedout = model.aspNetUser.LockoutEnabled;
            model.aspNetUserEmail = model.aspNetUser.Email;
            model.aspnetuserprofile = aspNetUserProfileMgr.GetById(userid);
            if (model.User.AspNetRolesId != null) model.AspNetUserRoleId = (int)model.User.AspNetRolesId;
            model.aspnetuserrole = aspnetuserrolemgr.GetByUserid(userid);
            var aspnetrolesmgr = new AspNetRoleManager();
            
           model.aspNetUserRoles = aspnetrolesmgr.GetList().Select(a => new SelectListItem
        {
             Value= a.Id.ToString(),
             Text = a.Name
        });
            

            return View("UserMaint",model);
        }

        [HttpPost]
        public ActionResult UserMaint(UserManagementModel model)
        {
            var aspNetUserMgr = new AspNetUsersManager();
            var aspNetUserProfileMgr = new AspNetUserProfilesManager();
            var aspnetuserroleMgr = new AspNetUserRolesManager();

            model.aspNetUser = aspNetUserMgr.GetById(model.userId);

            model.aspNetUser.Email = model.aspNetUserEmail;
            model.aspNetUser.LockoutEnabled = model.aspnetuserlockedout;

            model.aspnetuserrole = aspnetuserroleMgr.GetByUserid(model.userId);
            model.aspnetuserrole.RoleId = model.AspNetUserRoleId;

            string results = aspNetUserProfileMgr.updUser_UserProfile(model.aspNetUser, model.aspnetuserprofile, model.aspnetuserrole);

            if(results != String.Empty)
                CreateAlert(AlertType.Error, results);
            

            return RedirectToAction("Index");
        }

        private void UpdateUser(int userid)
        {


        }

    }
}
