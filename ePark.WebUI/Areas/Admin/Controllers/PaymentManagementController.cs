﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Mapping;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Windows.Forms;
using BnB.Common.Mail;
using ePark.Data;
using ePark.Managers;
using ePark.WebUI.App_Helpers;
using ePark.WebUI.Areas.Admin.Models;
using ePark.WebUI.Controllers;
using ePark.WebUI.Models;
using ePark.WebUI.FISPayment;
using ePark.WebUI.Models.Enums;
using ePark.WebUI.MvcExtensions;
using Microsoft.Ajax.Utilities;
using FormCollection = System.Web.Mvc.FormCollection;

namespace ePark.WebUI.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class PaymentManagementController : BaseController
    {
        public decimal cbidfee { get; set; }
        public decimal tax { get; set; }

        public decimal amt { get; set; }

        public decimal ttlamt { get; set; }

        public string localids { get; set; }

        // GET: Admin/PaymentManagement
        public ActionResult Index()
        {
            var paymentManagmentModel = new PayModel();
            var receiptmgr = new ReceiptManager();
            var vwPayments = new ReservationPaymentViewManager();

            paymentManagmentModel.ResPaymentViewList = vwPayments.GetList();
            paymentManagmentModel.ReceiptPayTypeList = receiptmgr.GetPayType();

            return View(paymentManagmentModel);
        }

        public JsonResult ProvidePayments(string sidx, string sord, int page, int rows, bool _search, string searchField, string searchOper, string searchString)
        {
            if (_search)
                GetSearchPayments(_search, searchField, searchOper, searchString);

            return Json(GetPayments(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult TransEdit(int id, string oper, decimal amount)
        {

            switch (oper)
            {
                case "edit":
                    saveContractTrans(id, amount);
                    break;
                default:
                    break;
            }

            string newoper = oper;
            decimal newamount = amount;

            return Json(GetPayments(), JsonRequestBehavior.AllowGet);

        }

        public Array GetSearchPayments(bool _search, string searchField, string searchOper, string searchString)
        {
            var usercontracts = new UserContractTransactionManager();
            var sysParmsForCalc = new SystemParametersManager();



            Array rows = usercontracts.GetUnPaidContractTrans().Select(x => new
            {
                x.ContractTransactionID,
                x.UserName,
                Name = x.FirstName.ToString() + " " + x.LastName.ToString(),
                x.GroupDesc,
                x.TransactionDesc,
                x.Amount
                //,CalcCbid = sysParmsForCalc.CalcCBID(x.Amount),
                //CalcTax = sysParmsForCalc.CalcTax(x.Amount),
                //ttlAmount = sysParmsForCalc.CalcCBID(x.Amount) + sysParmsForCalc.CalcTax(x.Amount) + x.Amount
            }).ToArray();

            return rows;

        }

        public Array GetPayments()
        {
            var usercontracts = new UserContractTransactionManager();
            var sysParmsForCalc = new SystemParametersManager();

            Array rows = usercontracts.GetUnPaidContractTrans().Select(x => new
            {
                x.ContractTransactionID,
                x.UserName,
                Name = x.FirstName.ToString() + " " + x.LastName.ToString(),
                x.GroupDesc,
                x.TransactionDesc,
                x.Amount
                //,CalcCbid = sysParmsForCalc.CalcCBID(x.Amount),
                //CalcTax = sysParmsForCalc.CalcTax(x.Amount),
                //ttlAmount = sysParmsForCalc.CalcCBID(x.Amount) + sysParmsForCalc.CalcTax(x.Amount) + x.Amount
            }).ToArray();

            return rows;
        }


        public ActionResult PaymentReceipt()
        {
     
            var paymentManagmentModel = new PayModel();
            var CustPaymentMgr = new vwCustomerPaymentsManager();

            paymentManagmentModel.CustomerReceipts = CustPaymentMgr.GetList();

            return View(paymentManagmentModel);
        }


        public ActionResult CustomerReceipt()
        {

            var paymentManagmentModel = new PayModel();
            var CustPaymentMgr = new vwCustomerPaymentsManager();

            paymentManagmentModel.CustomerReceipts = CustPaymentMgr.GetList();

            return View(paymentManagmentModel);
        }


        public JsonResult ProvideCustomerPayments(PayModel model)
        {
            return Json(GetCustomerPayments(model), JsonRequestBehavior.AllowGet);
        }


        public Array GetCustomerPayments(PayModel model)
        {
            var custpayments = new vwCustomerPaymentsManager();

            Array rows = custpayments.GetList().Select(x => new
            {
                x.Id,
                x.UserName,
                x.transResponseString,
                x.transDate,
                x.transAmount,
                x.transStatus,
                x.transTransactionID,
                x.transQuery,
                x.transUser3,
                x.TransReceiptID
            }).ToArray();

            return rows;
        }


        // GET: Admin/PaymentManagement/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // POST: Admin/PaymentManagement/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/PaymentManagement/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Admin/PaymentManagement/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/PaymentManagement/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Admin/PaymentManagement/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult PaymentMaint(string ids)
        {
            var regPayMgr = new ReservationPaymentViewManager();
            var UserCont = new UserContractTransactionManager();
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            var fispayment = new FISPaymentHelper();

            localids = ids;
            
            var openPayments = UserCont.GetTrans();
            var payMgtModel = new PayModel();

            if (ids == null) return View(payMgtModel);

            payMgtModel.CurrentPaymentList =
                openPayments.Where(p => ids.Contains(p.ContractTransactionID.ToString())).ToList();

            payMgtModel.userid = payMgtModel.CurrentPaymentList[0].AspNetUserId;

            payMgtModel.ContractTranstctionIds = ids;
            TempData["CurrentPayments"] = ids;

            // Pay Types CC, Check, Cashiers Check......
            payMgtModel.PayTypes = fispayment.GetPayTypes();
            payMgtModel.SelectedPayTypeId = 6;

            payMgtModel.BillingCardTypes = GetBillingCardTypes();
            payMgtModel.BillingCardTypesID = 1;

          //  payMgtModel.CCYearsItems = loadddlCCExpYear();
            payMgtModel.SelectedCCYearId = 1;

            var currPayment = PaymentPrep(ids);

            payMgtModel.ttlAmount = ttlamt;
            payMgtModel.ttlCBIDFee = cbidfee;
            payMgtModel.ttlTax = tax;
            payMgtModel.amt = amt;
            payMgtModel.ContractTranstctionIds = ids;

            return View(payMgtModel);
        }


        public ActionResult ReceiptbyReceiptId(int? receiptid)
        {
            var receiptmodel = new ReceiptModel();
            var AspNetuser = new AspNetUsersManager();
            var AspNetuserprofile = new AspNetUserProfilesManager();
            //var ResPaymentVWMgr = new ReservationPaymentViewManager();
            var contractMgr = new ContractManager();
            var receiptMgr = new ReceiptManager();

            receiptmodel.Currentdate = System.DateTime.Now.ToShortDateString();
            

                if (receiptid != null)
                {
                    receiptmodel.ReceiptPayments = ReceiptPaymentsbyReceiptID((int)receiptid); 
                    var currContract = contractMgr.GetById(receiptmodel.ReceiptPayments.FirstOrDefault().ContractID.Value);
                    if (currContract.AspNetUserID != null)
                        receiptmodel.aspNetUsers = AspNetuser.GetById((int)currContract.AspNetUserID);
                    if (receiptmodel.aspNetUsers.AspNetUserProfile_Id != null)
                        receiptmodel.aspnetuserprofile = AspNetuserprofile.GetById((int)receiptmodel.aspNetUsers.AspNetUserProfile_Id);

                    receiptmodel.currContract = currContract;

                    receiptmodel.CityStateZip = receiptmodel.aspnetuserprofile.Address1 + " " +
                                                receiptmodel.aspnetuserprofile.City + ", " +
                                                receiptmodel.aspnetuserprofile.State;

                    receiptmodel.ttlAmount = ttlamt;
                    receiptmodel.PaymentType = receiptmodel.ReceiptPayments[0].transQuery;

                     // if (TempData["PaymentProcessed"] != null)
                      //  {
                            MailManager.EmailPaymentReceipt(receiptmodel.aspnetuserprofile.Id,
                            receiptmodel.Currentdate, receiptmodel.ReceiptPayments, receiptmodel.ttlAmount,
                            String.Format("{0}{1}", receiptmodel.aspnetuserprofile.FirstName,
                            receiptmodel.aspnetuserprofile.LastName), receiptmodel.currContract.ContractID,
                            receiptmodel.aspnetuserprofile.Address1, receiptmodel.aspnetuserprofile.City,
                            receiptmodel.aspnetuserprofile.State, receiptmodel.aspnetuserprofile.PostalCode,
                            receiptmodel.aspnetuserprofile.PhonePrimary);
                      //  }
                }

               //Response.Redirect("www.google.com");


            return View("Receipt", receiptmodel);
                
        }
        

        protected List<vwPaymentReceipt> ReceiptPaymentsbyReceiptID(int receiptid)
        {
            cbidfee = 0;
            amt = 0;
            tax = 0;
            ttlamt = 0;
            List<vwPaymentReceipt> openPayments = new List<vwPaymentReceipt>();
            var regPayMgr = new vwPaymentReceiptsManager();
            var parmMgr = new SystemParametersManager();
            openPayments = regPayMgr.GetListByReceiptId(receiptid);

            if (TempData != null)
            {

                foreach (var trans in openPayments)
                {
                    trans.Tax = 0;
                    trans.CBIDFee = 0;
                    trans.ConvFee = 0;
                    if (!trans.IsTaxExempt && trans.ContractTransactionTypeID == 1)
                    {
                        trans.Tax = parmMgr.CalcTax(trans.Amount);
                        tax = (decimal)(tax + trans.Tax);
                    }

                    if (trans.ContractTransactionTypeID == 1)
                    {
                        trans.CBIDFee = parmMgr.CalcCBID(trans.Amount);
                        cbidfee = (decimal)(cbidfee + trans.CBIDFee);
                    }

                    amt = (decimal)(amt + trans.Amount);
                    trans.ConvFee = trans.Amount + trans.Tax + trans.CBIDFee;

                }

                ttlamt = ttlamt + amt + tax + cbidfee;
            }
            return openPayments;
        }



        public ActionResult PaymentTrans()
        {
            var payMgtModel = new PayModel();
            payMgtModel.CurrentPaymentList = TempData["CurrentPayments"] as List<vwUserContractTransaction>;

            return View("PaymentMaint");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> PaymentMaint(PayModel model)
        {
            var vwPayments = new ReservationPaymentViewManager();
            vwPayments.GetUnPaidForUserContractTrans(model.userid);
            model.receipt = new Receipt();
            var parmMgr = new SystemParametersManager();
            model.ResPaymentViewList = vwPayments.GetUnPaidForUserContractTrans(model.userid);

            TempData["ResViewList"] = model.ResPaymentViewList;
            model.receipt.ReceiptPayTypeID = model.SelectedPayTypeId;
            model.receipt.ReceiptID = saveReceipt(model);

            var errors = ModelState.Values.SelectMany(v => v.Errors);
            //if (ModelState.IsValid)
            //{
            cbidfee = 0;
            amt = 0;
            tax = 0;
            ttlamt = 0;

            foreach (vwReservationPayment contractTrans in model.ResPaymentViewList)
            {

                contractTrans.Tax = 0;
                contractTrans.CBIDFee = 0;
                contractTrans.ConvFee = 0;
                if (!contractTrans.IsTaxExempt && contractTrans.ContractTransactionTypeID == 1)
                {
                    contractTrans.Tax = parmMgr.CalcTax(contractTrans.Amount);
                    tax = (decimal)(tax + contractTrans.Tax);
                }

                if (contractTrans.ContractTransactionTypeID == 1)
                {
                    contractTrans.CBIDFee = parmMgr.CalcCBID(contractTrans.Amount);
                    cbidfee = (decimal)(cbidfee + contractTrans.CBIDFee);
                }

                amt = (decimal)(amt + contractTrans.Amount);
                contractTrans.ConvFee = contractTrans.Amount + contractTrans.Tax + contractTrans.CBIDFee;
                int contractresult = saveContract(model, contractTrans);


                ttlamt = amt + tax + cbidfee;
            }


            TempData["ttlamount"] = ttlamt;

            var pmttrans = new PaymentTransactionLog();
            model.receipt.BillingCardTypesID = null;
            model.receipt.ReceiptPayTypeID = model.SelectedPayTypeId;
            int receiptupdatestatus = updateReceipt(model);

            TempData["ReceiptID"] = model.receipt.ReceiptID;

            int cc = saveTransLog(model);


            // }

            TempData["PaymentProcessed"] = true;
            return RedirectToAction("ReceiptbyReceiptId", "PaymentManagement", new { receiptid = model.receipt.ReceiptID });

            //return RedirectToAction("Receipt", "PaymentManagement", new { ids = model.ContractTranstctionIds });

        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult PaymentCC(PayModel model)
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            var ReceiptMgr = new ReceiptManager();
            model.receipt = new Receipt();
            var parmMgr = new SystemParametersManager();
            var fispayhelper = new FISPaymentHelper();

            FISPayment.ApiService client = new FISPayment.ApiService();

            
            var paymentRequest = new FISPayment.SubmitPaymentRequest();

            if (ModelState.IsValid)
            {
              

                var AccessRequest = fispayhelper.AssesCardFees(model);
 
                try
                {
                    var assessfees = client.AssessFeesForCard(AccessRequest);
                    paymentRequest.TokenId = assessfees.TokenId;
                    // paymentRequest.ClientTransactionId = "1045";//model.ResPaymentView.ContractID.ToString();
                    paymentRequest.PaymentChargeTypeCode = "P";
                    paymentRequest.MerchantCode = "50BNA-NSVCC-MCCWF-G";

                    var SubmitResponse = client.SubmitPayment(paymentRequest);

                    string strMsg = SubmitResponse.Message.ToString();
                    string PaymentID = SubmitResponse.PaymentId;
                    string[] errmsg1 = SubmitResponse.ErrorMessages;


                    var paytrans = SubmitResponse.Transactions;


                    if (paytrans[0].RCString != "APPROVED")
                    {
                        CreateAlert(AlertType.Error, "Invalid Payment : " + paytrans[0].RCString);

                        int cc = saveTransLog(model, SubmitResponse);
                    }
                    else
                    {
                        model.receipt.ReceiptID = saveReceipt(model);

                        int ret = saveContractTrans(model);
                        
                        int cc = saveTransLog(model, SubmitResponse);

                        TempData["PaymentProcessed"] = true;
                        return RedirectToAction("ReceiptbyReceiptId", "PaymentManagement", new { receiptid = model.receipt.ReceiptID });
                    }
                }
                catch (Exception ex)
                {
                    CreateAlert(AlertType.Error, ex.Message);
                }

                
                // only returning here because the Index View of Payments returns a different view... I need to return this error message on this view but will do this temporarily.
               // return RedirectToAction("Index", "Dashboard", new { area = "Admin" });

            }
            return RedirectToAction("PaymentMaint", "PaymentManagement", new { ids = model.ContractTranstctionIds });

           // return RedirectToAction("Receipt", "PaymentManagement", new { ids = model.ContractTranstctionIds }); ;
        }

        public int saveContractTrans(PayModel model)
        {

            var parmMgr = new SystemParametersManager();

            cbidfee = 0;
            amt = 0;
            tax = 0;
            ttlamt = 0;

            foreach (vwReservationPayment contractTrans in currentPayments(model.ContractTranstctionIds))
            {
                if (model.CCard.Length > 10)
                {
                    contractTrans.Tax = 0;
                    contractTrans.CBIDFee = 0;
                    contractTrans.ConvFee = 0;

                    if (!contractTrans.IsTaxExempt && contractTrans.ContractTransactionTypeID == 1)
                    {
                        contractTrans.Tax = parmMgr.CalcTax(contractTrans.Amount);
                        tax = (decimal)(tax + contractTrans.Tax);
                    }

                    if (contractTrans.ContractTransactionTypeID == 1)
                    {
                        contractTrans.CBIDFee = parmMgr.CalcCBID(contractTrans.Amount);
                        cbidfee = (decimal)(cbidfee + contractTrans.CBIDFee);
                    }

                    amt = (decimal)(amt + contractTrans.Amount);
                    contractTrans.ConvFee = contractTrans.Amount + contractTrans.Tax + contractTrans.CBIDFee;

                    // save contracttransaction - TransReceiptXref
                    int contractresult = saveContract(model, contractTrans);

                }

                ttlamt = ttlamt + amt + tax + cbidfee;
            }

            return 0;

        }
      

        // the following populate jqgrid for payments
        public JsonResult ProvideSelectedPayments(PayModel model)
        {
            string newids = TempData["CurrentPayments"].ToString();
            return Json(GetSelectedPayments(newids), JsonRequestBehavior.AllowGet);
        }

        public Array GetSelectedPayments(string newids)
        {
            var currPayments = PaymentPrep(newids);

            Array rows = null;
            rows = currPayments.Select(x => new
            {
                ContractTransactionID = x.ContractTransactionID,
                UserName = x.UserName,
                FirstName = x.FirstName,
                LastName = x.LastName,
                TransactionDesc = x.TransactionDesc,
                Tax = x.Tax,
                CBIDFee = x.CBIDFee,
                Amount = x.Amount,

            }).ToArray();

            return rows;
        }

        protected List<vwReservationPayment> PaymentPrep(string newids)
        {
            cbidfee = 0;
            amt = 0;
            tax = 0;
            ttlamt = 0;
            List<vwReservationPayment> currPayments = new List<vwReservationPayment>();
            var regPayMgr = new ReservationPaymentViewManager();
            var parmMgr = new SystemParametersManager();
            //var openPayments = regPayMgr.GetUnPaidContractTrans();
            var openPayments = regPayMgr.GetList();

            if (TempData != null)
            {

                currPayments = openPayments.Where(p => newids.Contains(p.ContractTransactionID.ToString())).ToList();

                foreach (var trans in currPayments)
                {
                    if (!trans.IsTaxExempt && trans.ContractTransactionTypeID == 1)
                    {

                        trans.Tax = parmMgr.CalcTax(trans.Amount);
                        tax = (decimal)(tax + trans.Tax);
                    }

                    if (trans.ContractTransactionTypeID == 1)
                    {
                        trans.CBIDFee = parmMgr.CalcCBID(trans.Amount);
                        cbidfee = (decimal)(cbidfee + trans.CBIDFee);
                    }
                    
                    amt = (decimal)(amt + trans.Amount);

                }

                ttlamt = ttlamt + amt + tax + cbidfee;
            }

            return currPayments;
        }


        protected List<vwReservationPayment> ReceiptPayments(int contractid)
        {
            cbidfee = 0;
            amt = 0;
            tax = 0;
            ttlamt = 0;
            List<vwReservationPayment> openPayments = new List<vwReservationPayment>();
            var regPayMgr = new ReservationPaymentViewManager();
            var parmMgr = new SystemParametersManager();
            openPayments = regPayMgr.GetByContractId(contractid);

            if (TempData != null)
            {

                foreach (var trans in openPayments)
                {
                    trans.Tax = 0;
                    trans.CBIDFee = 0;
                    trans.ConvFee = 0;
                    if (!trans.IsTaxExempt && trans.ContractTransactionTypeID == 1)
                    {
                        trans.Tax = parmMgr.CalcTax(trans.Amount);
                        tax = (decimal)(tax + trans.Tax);
                    }

                    if (trans.ContractTransactionTypeID == 1)
                    {
                        trans.CBIDFee = parmMgr.CalcCBID(trans.Amount);
                        cbidfee = (decimal)(cbidfee + trans.CBIDFee);
                    }

                    amt = (decimal)(amt + trans.Amount);
                    trans.ConvFee = trans.Amount + trans.Tax + trans.CBIDFee;

                }

                ttlamt = ttlamt + amt + tax + cbidfee;
            }
            return openPayments;
        }


        protected List<vwReservationPayment> ReceiptPayments(string ids)
        {
            cbidfee = 0;
            amt = 0;
            tax = 0;
            ttlamt = 0;
            List<vwReservationPayment> openPayments = new List<vwReservationPayment>();
            var regPayMgr = new ReservationPaymentViewManager();
            var parmMgr = new SystemParametersManager();
            openPayments = regPayMgr.GetPaidForUserContractTrans();

            openPayments = openPayments.Where(p => ids.Contains(p.ContractTransactionID.ToString())).ToList();

           // openPayments = regPayMgr.GetUnPaidForUserContractTrans(ids);
            if (TempData != null)
            {

                foreach (var trans in openPayments)
                {
                    trans.Tax = 0;
                    trans.CBIDFee = 0;
                    trans.ConvFee = 0;
                    if (!trans.IsTaxExempt && trans.ContractTransactionTypeID == 1)
                    {
                        trans.Tax = parmMgr.CalcTax(trans.Amount);
                        tax = (decimal)(tax + trans.Tax);
                    }

                    if (trans.ContractTransactionTypeID == 1)
                    {
                        trans.CBIDFee = parmMgr.CalcCBID(trans.Amount);
                        cbidfee = (decimal)(cbidfee + trans.CBIDFee);
                    }

                    amt = (decimal)(amt + trans.Amount);
                    trans.ConvFee = trans.Amount + trans.Tax + trans.CBIDFee;

                }

                ttlamt = ttlamt + amt + tax + cbidfee;
            }
            return openPayments;
        }

        private List<vwReservationPayment> currentPayments(string contractTransList)
        {
            var regPayMgr = new ReservationPaymentViewManager();
            var openPayments = regPayMgr.GetOpenContractTransList();

            openPayments = openPayments.Where(p => contractTransList.Contains(p.ContractTransactionID.ToString())).ToList();

            return openPayments;
        }

        private List<vwReceipt> selectedPaymentsbyReceiptID(int receiptid)
        {
            var regPayMgr = new vwReceiptsManager();

            var selectedPayments = regPayMgr.GetListByReceiptId(receiptid);

            return selectedPayments;
        }


        private List<vwReservationPayment> selectedPayments(int contractid)
        {
            var regPayMgr = new ReservationPaymentViewManager();

            var selectedPayments = regPayMgr.GetByContractId(contractid);

            return selectedPayments;
        }

        private List<vwReservationPayment> selectedPayments(string contractTransList)
        {
            var regPayMgr = new ReservationPaymentViewManager();
            var selectedPayments = regPayMgr.GetList();

            selectedPayments = selectedPayments.Where(p => contractTransList.Contains(p.ContractTransactionID.ToString())).ToList();

            return selectedPayments;
        }

        private IEnumerable<SelectListItem> GetBillingCardTypes()
        {
            var syspaytypes = new ReceiptManager();

            var billingcardtypes = syspaytypes.GetBillingCardType().Select(x =>
                                new SelectListItem
                                {
                                    Value = x.BillingCardTypesID.ToString(),
                                    Text = x.CardDesc
                                });

            return new SelectList(billingcardtypes, "Value", "Text");

        }


        private IEnumerable<SelectListItem> loadddlCCExpYear()
        {

            var selectList =
    new SelectList(Enumerable.Range(System.DateTime.Now.Year, 12));

            return new SelectList(selectList, "Value", "Text");
        }

        private int saveTransLog(PayModel model, SubmitPaymentResponse payresp)
        {
            var translog = new PaymentTransactionLog();
            var translogMgr = new PaymentTransactionsLogManager();
            var recepitMgr = new ReceiptManager();
            var parmMgr = new SystemParametersManager();
            var contractMgr = new ContractManager();

            //var resptrans = new SubmitPaymentResponse().Transactions;
            var resptrans = payresp.Transactions;
            translog.transDate = System.DateTime.Now;

            // transLogID
            model.receipt.ReceiptPayTypeID = 5;

            string paytype = recepitMgr.GetPaymentType(model.receipt);
            translog.transQuery = paytype;

            BillingCardType billingCardType = recepitMgr.GetBillingCardType().FirstOrDefault(x => x.BillingCardTypesID == model.BillingCardTypesID);
            if (billingCardType !=
                null)
                translog.transCardType = billingCardType.CardDesc;

            translog.transRequestType = "PROD";
            translog.transMerchantCode = "50BNA-EBIDS-EBIDS-G";
            translog.transSettleCode = "50BNA-EBIDS-EBIDS-00";
            if (model.CCard != null)
                translog.transLast5Digits = Convert.ToInt32(model.CCard.Substring((model.CCard.Length - 5), 5));
            translog.transAmount = Convert.ToDouble(model.ttlAmount);
            // translog.transConvFee = parmMgr.CalcCBID(contractTrans.Amount);
            translog.transExpMM = model.CCMonthSelectedId;
            translog.transExpYY = model.SelectedCCYearId;
            translog.transCardSecurityValue = model.CVV;
            translog.transBillAddressSent = true;
            //transDataSent  FROM EPP

            if (resptrans != null)
            {
                translog.transReturnCode = resptrans[0].RC;
                translog.transStatus = resptrans[0].Message;
                translog.transResponseString = resptrans[0].RCString;
                translog.transTransactionID = resptrans[0].TransID.ToString();
            }

            translog.transDateStamp = System.DateTime.Now;
            translog.CBIDFee = model.ttlCBIDFee;
            translog.Tax = model.ttlTax;
            translog.transUser3 = contractMgr.GetByAspNetUserId(model.userid).ContractID.ToString();
            translog.UserId = model.userid;
            translog.TransReceiptID = model.receipt.ReceiptID;

            translogMgr.Create(translog);

            return 0;
        }

        private int saveTransLog(PayModel model)
        {
            var translog = new PaymentTransactionLog();
            var translogMgr = new PaymentTransactionsLogManager();
            var recepitMgr = new ReceiptManager();
            var parmMgr = new SystemParametersManager();
            var contractMgr = new ContractManager();

            //var resptrans = new SubmitPaymentResponse().Transactions;
            //var resptrans = payresp.Transactions;
            translog.transDate = System.DateTime.Now;

            // transLogID

            string paytype = recepitMgr.GetPaymentType(model.receipt);
            translog.transQuery = paytype;

            BillingCardType billingCardType = recepitMgr.GetBillingCardType().FirstOrDefault(x => x.BillingCardTypesID == model.BillingCardTypesID);
            if (billingCardType !=
                null)
                translog.transCardType = billingCardType.CardDesc;

            translog.transRequestType = "PROD";
            translog.transMerchantCode = "50BNA-EBIDS-EBIDS-G";
            translog.transSettleCode = "50BNA-EBIDS-EBIDS-00";
            if (model.CCard != null)
                translog.transLast5Digits = Convert.ToInt32(model.CCard.Substring((model.CCard.Length - 5), 5));
            translog.transAmount = Convert.ToDouble(model.ttlAmount);
            // translog.transConvFee = parmMgr.CalcCBID(contractTrans.Amount);
            translog.transExpMM = model.CCMonthSelectedId;
            translog.transExpYY = model.SelectedCCYearId;
            translog.transCardSecurityValue = model.CVV;
            translog.transBillAddressSent = true;
            //transDataSent  FROM EPP

            //if (resptrans != null)
            //{
            //    translog.transReturnCode = resptrans[0].RC;
            //    translog.transStatus = resptrans[0].Message;
            //    translog.transResponseString = resptrans[0].RCString;
            //    translog.transTransactionID = resptrans[0].TransID.ToString();
            //}

            translog.transDateStamp = System.DateTime.Now;
            translog.CBIDFee = model.ttlCBIDFee;
            translog.Tax = model.ttlTax;
            translog.transUser3 = contractMgr.GetByAspNetUserId(model.userid).ContractID.ToString();
            translog.UserId = model.userid;
            translog.TransReceiptID = model.receipt.ReceiptID;

            translogMgr.Create(translog);

            return 0;
        }

        private int saveContract(PayModel model, vwReservationPayment contractTrans)
        {
            var contracttransmgr = new ContractTransactionManager();
            var contract = new ContractTransaction();
            var ContractreceiptXrefMgr = new ContractTransactionReceiptXrefManager();

            contract.ContractTransactionID = (int)contractTrans.ContractTransactionID;
            contract.ContractID = (int)contractTrans.ContractID;
            contract.ContractTransactionTypeID = contractTrans.ContractTransactionTypeID;
            contract.Paid = true;
            contract.Amount = contractTrans.Amount;
            contract.ReservedMonth = contractTrans.ReservedMonth;
            contract.ReservedMonthDateTime = contractTrans.ReservedMonthDateTime;
            contract.CreatedDate = contractTrans.CreatedDate;
            contract.GroupID = contractTrans.GroupID;

            saveTransReceiptXref(model, contractTrans);

            return contracttransmgr.Update(contract);

        }

        private int saveReceipt(PayModel model)
        {
            var receiptMgr = new ReceiptManager();
            var Receipt = new Receipt();

            Receipt.BillingCardTypesID = model.BillingCardTypesID;
            Receipt.ReceiptPayTypeID = model.SelectedPayTypeId;
            Receipt.ReceiptTotalAmount = model.ttlAmount;
            Receipt.CBIDFee = model.ttlCBIDFee;
            Receipt.Tax = model.ttlTax;
            Receipt.PayDate = System.DateTime.Now;
            Receipt.ReceiptPayStatusID = 2;
            
            return receiptMgr.Create(Receipt);

        }


        private int updateReceipt(PayModel model)
        {
            var receiptMgr = new ReceiptManager();
            var Receipt = new Receipt();

            Receipt = receiptMgr.GetById(model.receipt.ReceiptID);

            Receipt.BillingCardTypesID = model.BillingCardTypesID;
            Receipt.ReceiptPayTypeID = model.SelectedPayTypeId;
            Receipt.ReceiptTotalAmount = model.ttlAmount;
            Receipt.CBIDFee = model.ttlCBIDFee;
            Receipt.Tax = model.ttlTax;
            Receipt.PayDate = System.DateTime.Now;
            Receipt.ReceiptPayStatusID = 2;

            return receiptMgr.Update(Receipt);
        }

        private void saveTransReceiptXref(PayModel model, vwReservationPayment trans)
        {
            var ContractreceiptXrefMgr = new ContractTransactionReceiptXrefManager();
            var ContractTransactionReceiptXref = new ContractTransactionReceiptXref();

            ContractTransactionReceiptXref.ReceiptID = model.receipt.ReceiptID;
            ContractTransactionReceiptXref.ContractTransactionID = trans.ContractTransactionID;
            ContractTransactionReceiptXref.CreateDate = System.DateTime.Now;
            ContractTransactionReceiptXref.IsActive = true;

            ContractreceiptXrefMgr.Create(ContractTransactionReceiptXref);
        }

        private void saveContractTrans(int TransID, decimal amount)
        {
            var contracttransmgr = new ContractTransactionManager();
            var existingContractTransaction = contracttransmgr.GetById(TransID);

            existingContractTransaction.Amount = amount;

            contracttransmgr.Update(existingContractTransaction);

        }
   
    }

}
