﻿using ePark.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ePark.WebUI.Areas.Users.ViewModels
{
    public class ParticipantViewModel
    {
        public Participant Participant { get; set; }
        public Contract Contract { get; set; }
    }
}