﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ePark.WebUI.Areas.Admin.Models;
using ePark.Managers;
using ePark.WebUI.Areas.Admin.ViewModels;
using ePark.WebUI.Areas.Users.ViewModels;
using ePark.WebUI.Controllers;
using ePark.WebUI.Helpers;
using ePark.WebUI.Models;
using Microsoft.AspNet.Identity.Owin;

namespace ePark.WebUI.Areas.Users.Controllers
{
    [Authorize(Roles = "Public")]
    public class DashboardController : BaseController
    {
        // GET: Admin/Dashboard
        public ActionResult Index(int? id)
        {
            var userDashboardViewModel = new UserDashboardViewModel();
            var inquiryManager = new InquiryManager();
            var vwPayments = new ReservationPaymentViewManager();
            var custpayments = new vwCustomerPaymentsManager();
            var custContracts = new vwContractManager();

            //Fetch all the inquiries (both approved and not) withing the last x amount of days
            var allRecentInquiries = inquiryManager.GetViewList(365);

            AspNetUserProfile aspnetUserProfile = null;
            if (Session["UserProfileModel"] != null)
            {
                aspnetUserProfile = (AspNetUserProfile)Session["UserProfileModel"];
                id = aspnetUserProfile.Id;
            }

            userDashboardViewModel.CurrentUserId = aspnetUserProfile.Id;

            //These are unapproved inquiries waiting to be evaluated
            userDashboardViewModel.InquiriesAwaitingPayment = allRecentInquiries.Where(x => x.AspNetUserProfilesId == userDashboardViewModel.CurrentUserId).ToList();

            if (id != null) userDashboardViewModel.CurrentPaymentViewList = vwPayments.GetUnPaidForUserContractTrans((int)id);

            if (id != null) userDashboardViewModel.AllPaymentViewList = vwPayments.GetListByUserId((int)id);

            if (id != null) userDashboardViewModel.CurrentReceiptView = custpayments.GetListByAspNetUserId((int)id);

            if (id != null) userDashboardViewModel.CurrentContracts = custContracts.GetListByUserId((int)id);

            //GetByUserId

            return View(userDashboardViewModel);
        }


        public JsonResult ProvidePayments(PayModel model)
        {
            return Json(GetPayments(model), JsonRequestBehavior.AllowGet);
        }

        public Array GetPayments(PayModel model)
        {
            var usercontracts = new UserContractTransactionManager();
            var sysParmsForCalc = new SystemParametersManager();

            Array rows = usercontracts.GetUnPaidContractTrans().Select(x => new
            {
                x.ContractTransactionID,
                x.UserName,
                x.FirstName,
                x.LastName,
                x.TransactionDesc,
                x.Amount
                //,CalcCbid = sysParmsForCalc.CalcCBID(x.Amount),
                //CalcTax = sysParmsForCalc.CalcTax(x.Amount),
                //ttlAmount = sysParmsForCalc.CalcCBID(x.Amount) + sysParmsForCalc.CalcTax(x.Amount) + x.Amount
            }).ToArray();

            return rows;
        }


    }
}
