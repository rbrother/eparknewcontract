﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ePark.Data;
using ePark.Managers;
using Microsoft.AspNet.Identity;

namespace ePark.WebUI.Models
{
    public class PayModel
    {

        // Payment Hist 
        [DisplayName("User")]
        public string User { get; set; }



        [DisplayName("Payment Date")]
        public string PaymentDate { get; set; }
        //Payment Hist

        public int userid { get; set; }

        public Vehicle currVehicle { get; set; }

        public List<Vehicle> listVehicles { get; set; }

        public IEnumerable<Vehicle> vehiclelist { get; set; }

        public Participant currParticipant { get; set; }

        public List<Participant> listParticipants { get; set; }
        
        public Contract userContract { get; set; }

        public string Currentdate { get; set; }

        [DisplayName("Total Amount")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal ttlAmount { get; set; }

        [DisplayName("Total CBID Fee")]
        public decimal ttlCBIDFee { get; set; }

        [DisplayName("Total Tax")]
        public decimal ttlTax { get; set; }

        [DisplayName("Amount")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal amt { get; set; }

        [DisplayName("Payment Type")]
        public int SelectedPayTypeId { get; set; }
        public IEnumerable<SelectListItem> PayTypes { get; set; }
        
        [DisplayName("Card Type")]
        public int BillingCardTypesID { get; set; }
        public IEnumerable<SelectListItem> BillingCardTypes { get; set; }

        [DisplayName("Card Exp Month")]
        public int CCMonthSelectedId { get; set; }
        public IEnumerable<SelectListItem> CCMonthItems
        {
            get
            {
                return new[]
            {
                new SelectListItem { Value = "01", Text = "01" },
                new SelectListItem { Value = "02", Text = "02" },
                new SelectListItem { Value = "03", Text = "03" },
                new SelectListItem { Value = "04", Text = "04" },
                new SelectListItem { Value = "05", Text = "05" },
                new SelectListItem { Value = "06", Text = "06" },
                new SelectListItem { Value = "07", Text = "07" },
                new SelectListItem { Value = "08", Text = "08" },
                new SelectListItem { Value = "09", Text = "09" },
                new SelectListItem { Value = "10", Text = "10" },
                new SelectListItem { Value = "11", Text = "11" },
                new SelectListItem { Value = "12", Text = "12" },
            };
            }
        }

        [DisplayName("Card Exp Year")]
        public int SelectedCCYearId { get; set; }
        public IEnumerable<SelectListItem> CCYearsItems
        {
            get
            {
                return new[]
            {
                new SelectListItem { Value = "2015", Text = "2015" },
                new SelectListItem { Value = "2016", Text = "2016" },
                new SelectListItem { Value = "2017", Text = "2017" },
                new SelectListItem { Value = "2018", Text = "2018" },
                new SelectListItem { Value = "2019", Text = "2019" },
                new SelectListItem { Value = "2020", Text = "2020" },
                new SelectListItem { Value = "2021", Text = "2021" },
                new SelectListItem { Value = "2022", Text = "2022" },
                new SelectListItem { Value = "2023", Text = "2023" },
                new SelectListItem { Value = "2024", Text = "2024" },
                new SelectListItem { Value = "2025", Text = "2025" },
            };
            }
        }

        //  public IEnumerable<SelectListItem> CCYearsItems { get; set; }


        [MinLength(13, ErrorMessage = "{0} must be at least {1} characters long")]
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "{0} should consist only of numbers")]
        [Required(ErrorMessage = "Credit Card Number is required.")]
        [DisplayName("Credit Card Number")]
        public string CCard { get; set; }

        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "{0} should consist only of numbers")]
        [DisplayName("Card Validation")]
        [Required(ErrorMessage = "Card Verification Value is required.")]
        public string CVV { get; set; }

        [DisplayName("Card Expiration")]
        public string CCExpMonth { get; set; }

        public string CCExpYear { get; set; }

        [Required(ErrorMessage = "Name on Card Value is required.")]
        [DisplayName("Name on Card")]
        public string CCName { get; set; }

        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "{0} should consist only of numbers")]
        [Required(ErrorMessage = "Zip Code Value is required.")]
        [DisplayName("Zip")]
        public string CCZip { get; set; }

        public string ContractTranstctionIds { get; set; }

        public List<vwReservationPayment> ResPaymentViewList { get; set; }
        public List<vwUserContractTransaction> CurrentPaymentList { get; set; }

        public Receipt receipt
        {
            get;
            set;
        }

        public List<Receipt> ReceiptList
        {
            get;
            set;
        }

        public List<ReceiptPayType> ReceiptPayTypeList
        {
            get;
            set;
        }

        public ContractTransaction Contracttransaction
        {
            get;
            set;
        }

        public vwReservationPayment ResPaymentView { get; set; }

        public PaymentTransactionLog TranslogLog { get; set; }

        public List<vwCustomerPayment> CustomerReceipts
        {
            get;
            set;
        }
        
 


    }
}